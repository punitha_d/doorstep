import { takeEvery, takeLatest } from 'redux-saga/effects';
import { 
  registerSaga,
  loginSaga,
  categoriesSaga,
  citiesListSaga,
  aboutusSaga,
  brandsListSaga,
  productsListSaga,
  productsInfoSaga,
  addTocartSaga,
  bankOfferSaga,
  getProfileSaga,
  editProfileSaga,
  cartListSaga,
  faqsSaga,
  contactusSaga,
  basketListSaga,
  deleteCartSaga,
  orderStatusSaga,
  orderListSaga,
  orderExistSaga,
  cartupdateSaga
} from './authenticationSaga';

import * as types from '../actions/types';


export default function* watchUserAuthentication() {
  yield takeLatest(types.REGISTER_USER, registerSaga);
  yield takeLatest(types.LOGIN_USER, loginSaga);
  yield takeLatest(types.CATEGORIES_LIST, categoriesSaga);
  yield takeLatest(types.CITIES_LIST, citiesListSaga);
  yield takeLatest(types.BRANDS_LIST, brandsListSaga);
  yield takeLatest(types.ABOUTUS, aboutusSaga);
  yield takeLatest(types.CONTACTUS, contactusSaga);
  yield takeLatest(types.FAQS, faqsSaga);
  yield takeLatest(types.PRODUCTS_LIST, productsListSaga);
  yield takeLatest(types.PRODUCTSINFO_LIST, productsInfoSaga);
  yield takeEvery(types.ADDTOCART_LIST, addTocartSaga);
  yield takeLatest(types.BANK_OFFER, bankOfferSaga);
  yield takeLatest(types.USER_PROFILE, getProfileSaga);
  yield takeLatest(types.EDIT_PROFILE, editProfileSaga);
  yield takeLatest(types.CART_LIST, cartListSaga);
  yield takeLatest(types.BASKET_LIST, basketListSaga);
  yield takeLatest(types.CARTDELETE_LIST, deleteCartSaga);
  yield takeLatest(types.ORDERSTATUS_LIST, orderStatusSaga);
  yield takeLatest(types.ORDER_LIST, orderListSaga);
  yield takeLatest(types.ORDER_EXIST, orderExistSaga);
  yield takeLatest(types.CART_UPDATE, cartupdateSaga);

}