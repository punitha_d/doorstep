import { put, call } from 'redux-saga/effects';
import { registerUserService, 
  loginUserService, 
  categoriesListService, 
  citiesListService, 
  aboutusService, 
  brandsListService,
  productsListService,
  productsInfoService,
  addTocartService,
  bankOfferService,
  getProfileService,
  editProfileService,
  cartListService,
  faqsService,
  contactusService,
  basketListService,
  deleteCartService,
  orderStatusService,
  orderListService,
  orderExistService,
  cartupdateService

} from '../services/authenticationService';
import * as types from '../actions/types'

export function* registerSaga(payload) {
  try {
    const response = yield call(registerUserService, payload);
    yield put({ type: types.REGISTER_USER_SUCCESS, response });
  } catch(error) {
    yield put({ type: types.REGISTER_USER_ERROR, error });
  }
}
export function* loginSaga(payload) {
  try {
    const response = yield call(loginUserService, payload);
    yield put({ type: types.LOGIN_USER_SUCCESS, response });
    localStorage.setItem('user', JSON.stringify(response));
  } catch(error) {
    const response = error.response;
    yield put({ type: types.LOGIN_USER_ERROR, payload: response });
  }
}

export function* categoriesSaga() {
  try {
    const categories = yield call(categoriesListService);
    yield put({ type: types.CATEGORIES_LIST_SUCCESS,  payload: categories.data });
  } catch(error) {
    yield put({ type: types.CATEGORIES_LIST_ERROR, error });
  }
}

//About us page
export function* aboutusSaga(){
  try{
    const aboutus = yield call(aboutusService);
    yield put({type: types.ABOUTUS_SUCCESS, payload:aboutus.data})
  }catch(error){
    yield put({type: types.ABOUTUS_ERROR, error})
  }
}

//CONTACT us page
export function* contactusSaga(){
  try{
    const contactus = yield call(contactusService);
    yield put({type: types.CONTACTUS_SUCCESS, payload:contactus.data})
  }catch(error){
    yield put({type: types.CONTACTUS_ERROR, error})
  }
}


//FAQs page
export function* faqsSaga(){
  try{
    const faqs = yield call(faqsService);
    yield put({type: types.FAQS_SUCCESS, payload:faqs.data})
  }catch(error){
    yield put({type: types.FAQS_ERROR, error})
  }
}


//Footer Cities we serve saga

export function* citiesListSaga(){
  try{
    const cities = yield call(citiesListService);
    yield put({ type: types.CITIES_LIST_SUCCESS, payload:cities.data });
  }catch(error){
    yield put({ type: types.CITIES_LIST_ERROR, error });
  }
}

export function* brandsListSaga(){
  try{
    const brands = yield call(brandsListService);
    yield put({ type: types.BRANDS_LIST_SUCCESS, payload:brands.data });
  }catch(error){
    yield put({ type: types.BRANDS_LIST_ERROR, error });
  }
}


export function* productsListSaga() {
  try {
    const productlist = yield call(productsListService);
    yield put({ type: types.PRODUCTS_LIST_SUCCESS,  payload: productlist.data });
  } catch(error) {
    yield put({ type: types.PRODUCTS_LIST_ERROR, error });
  }
}
//cart list

export function* cartListSaga() {
  try {
    const cartlist = yield call(cartListService);
    yield put({ type: types.CART_LIST_SUCCESS, payload:cartlist.data });
  } catch(error) {
    yield put({ type: types.CART_LIST_ERROR, error });
  }
}

//cart update list

export function* cartupdateSaga(payload) {
  try {
    const cartupdate = yield call(cartupdateService, payload);
    yield put({ type: types.CART_UPDATE_SUCCESS, payload:cartupdate.data });
    const cartlist = yield call(cartListService);
    yield put({ type: types.CART_LIST_SUCCESS, payload:cartlist.data });
  } catch(error) {
    yield put({ type: types.CART_UPDATE_ERROR, error });
  }
}

//Single product detail page

export function* productsInfoSaga() {
  try {
    const response = yield call(productsInfoService);
    yield put({ type: types.PRODUCTSINFO_LIST_SUCCESS,  payload: response.data });
  } catch(error) {
    yield put({ type: types.PRODUCTSINFO_LIST_ERROR, error });
  }
}


//Add to cart page

export function* addTocartSaga(payload ) {
  try {
    const addtocart = yield call(addTocartService, payload);
    yield put({ type: types.ADDTOCART_LIST_SUCCESS, payload:addtocart.data }); 
    const basket = yield call(basketListService);
    yield put({type: types.BASKET_LIST_SUCCESS, payload:basket.data})
    const orderexist = yield call(orderExistService );
    yield put({ type: types.ORDER_EXIST_SUCCESS, payload:orderexist.data });
    payload.onSuccess(orderexist.data)
  } catch(error) {
    yield put({ type: types.ADDTOCART_LIST_ERROR, error });
  }
}
//Delete cart page

export function* deleteCartSaga(payload) {

  try {
    const deletepro = yield call(deleteCartService, payload);
    yield put({ type: types.CARTDELETE_LIST_SUCCESS, payload:deletepro });
    const basket = yield call(basketListService);
    yield put({type: types.BASKET_LIST_SUCCESS, payload:basket.data})
    const cartlist = yield call(cartListService);
    yield put({ type: types.CART_LIST_SUCCESS, payload:cartlist.data });
  } catch(error) {
    yield put({ type: types.CARTDELETE_LIST_ERROR, error });
  }
}

//BASKET page
export function* basketListSaga(){
  try{
    const basket = yield call(basketListService);
    yield put({type: types.BASKET_LIST_SUCCESS, payload:basket.data})
  }catch(error){
    yield put({type: types.BASKET_LIST_ERROR, error})
  }
}


//About us page
export function* bankOfferSaga(){
  try{
    const bankoffer = yield call(bankOfferService);
    yield put({type: types.BANK_OFFER_SUCCESS, payload:bankoffer})
  }catch(error){
    yield put({type: types.BANK_OFFER_ERROR, error})
  }
}


export function* getProfileSaga(payload) {
  try {
    const getprofile = yield call(getProfileService, payload);
    yield put({ type: types.USER_PROFILE_SUCCESS, payload:getprofile.data });
  } catch(error) {
    yield put({ type: types.USER_PROFILE_ERROR, error });
  }
}


export function* editProfileSaga(payload) {
  try {
    const response = yield call(editProfileService, payload);
    yield put({ type: types.EDIT_PROFILE_SUCCESS, payload:response });
  } catch(error) {
    yield put({ type: types.EDIT_PROFILE_ERROR, payload:error });
  }
}
export function* orderListSaga() {
  try {
    const orderlist = yield call(orderListService, );
    yield put({ type: types.ORDER_LIST_SUCCESS, payload:orderlist });
  } catch(error) {
    yield put({ type: types.ORDER_LIST_ERROR, error });
  }
}
export function* orderExistSaga(action) {
  try {
    const orderexist = yield call(orderExistService );
    yield put({ type: types.ORDER_EXIST_SUCCESS, payload:orderexist.data });
    action.onSuccess(orderexist.data);
  } catch(error) {
    yield put({ type: types.ORDER_EXIST_ERROR, error });
  }
}
export function* orderStatusSaga(payload) {
  try {
    const order = yield call(orderStatusService, payload);
    yield put({ type: types.ORDERSTATUS_LIST_SUCCESS, payload:order });
    const orderlist = yield call(orderListService, );
    yield put({ type: types.ORDER_LIST_SUCCESS, payload:orderlist });
    const basket = yield call(basketListService);
    yield put({type: types.BASKET_LIST_SUCCESS, payload:basket.data})

  } catch(error) {
    yield put({ type: types.ORDERSTATUS_LIST_ERROR, error });
  }
}

