import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './store/configureStore';
import './index.css';

import App from './App';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));

  const register =()=> {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function () {
        navigator.serviceWorker.register('../public/sw').then(function (registration) {
          console.log('Worker registration is successful', registration.scope);
        }, function (err) {
          console.log('Worker registration has failed', err);
        }).catch(function (err) {
          console.log("err", err);
        });
      });
    } else {
      console.log('Service Worker is not supported by your browser.');
    }
  }
  const unregister=()=> {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.ready
        .then(registration => {
          registration.unregister();
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
  register();