import STRINGS from "./strings";
import MESSAGE from "./errormessage";
import REGEX from "./regexpattern";
export {
    STRINGS, MESSAGE, REGEX
  };
  