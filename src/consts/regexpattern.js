export default {
    EMAIL: /^[a-zA-Z0-9_+-]+[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(statefarm|ymedialabs)\.com$/i,
};
  