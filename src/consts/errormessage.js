export default {
    EMPTY_FIELD: "*This field is required.",
    VALID_EMAIL: "*Please provide a valid email address.",
};
  