export default {
  HEADER:{
    LOGO_TITLE: "Door Step",
    LOGIN: "Sign In",
    LOGOUT: "Log Out",
    REGISTER: "Sign Up",
    SHOPBYCATEGORY: "Shop By Category",
    RESET: "Reset",
    MYACCOUNT: "My Account"
  },
  LOGIN:{
    LOGINERROR: "Username / Password invalid",
  },
  HOMEPAGE:{
    BANKOFFER: "Bank Offer",
  },
  FORM: {
    FIRSTNAME: "First name",
    LASTNAME: "Last name",
    CITY: "City",
    STATE: "State",
    COUNTRY: "Country",
    USERNAME: "User name",
    EMAIL: "Email",
    MOBILE: "Mobile No",
    STREET: "Street",
    PASSWORD: "Password",
    CONFIRMPASSWORD: "Confirm Password"
  },
  FOOTER:{
    PAYMENT: "Payment Methods",
    CATEGORY: "Popular Category",
    BRANDS: "Popular Brands",
    CITIESWESERVE: "Cities We Serve",
    ABOUTUS: "About Us",
    PRIVACY: "Privacy Policy",
    TERMS: "Terms and Conditions",
    NEWS: "Doorstep News",
    FAQ: "FAQs",
    CONTACTUS: "Contact US",
    COPYRIGHT_TEXT: "© Copyright, YMedia labs pvt ltd, 2020.",
    FOOTERONE:"Doorstep",
    HELP:"Help",
    DOWNLOADAPP:"Download Our App",
    SOCIAL:"Get Social With us",
  },
  DASHBOARD:{
    WELCOMEMESSAGE: "Welcome to Dashboard",
    ACCOUNTS: "My Account",
    EDIT: "Edit Profile",
    CART: "My Cart",
    ORDERHISTORY: "Order History",
    CARTLIST: "List of items in cart",
    ORDER: "Order History",
    CANCELORDER:"Cancel Order"

  },
  SUCCESS:{
    EDIT_MSG:"Profile Updated Successfully",
    CART_MSG:"Successfully added to the basket",
    CART_UPDATE_MSG:"Cart Updated Successfully",
    CART_DELETE:"Item removed from the cart successfully",
    ORDER_STATUS:"Ordered successfully"
  },
  PRODUCT:{
    TAX:"(Inclusive of all taxes)"
  },
  PAGES:{
    FAQS:"Frequently Asked Questions",
    CONTACTUS:"Contact Us"
  },
  CHECKOUT:{
    DELIVERY:"Delivery Address",
    PAYMENT:"Payment Method",
    COD:"COD((Default Payment Method))",
    TOTAL:"Total",
    CHECKOUT:"Checkout",
    EMPTY:"Cart is empty"
  }
  
};
  