import configureStore from 'redux-mock-store'
import { render, fireEvent, cleanup } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect'
import renderer from 'react-test-renderer'
import Login, { Props } from '../loginPage'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import {initialState} from '../../../reducers/loginReducer';

const mockStore = configureStore();
let store;

store = mockStore(initialState)

describe("Login component", ()=>{
    afterEach(cleanup);

    test("Renders without creashing", () => {
        render(
            <Provider store={store}>
                <BrowserRouter>
                    <Login />
                </BrowserRouter>
            </Provider>
        );
    });
    test("creates Snapshot", () => {
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Login onSubmit = {jest.fn}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
    test("onSubmit Button should mount", () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Login />
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("button");
        expect(button).toBeInTheDocument();
        expect(button).toHaveClass("submitbutton");
    });
    test('should display a blank login form', () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>  
                    <Login onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("login-form");
        fireEvent.click(button);
        expect(button).toHaveFormValues({
            identifier: "",
            password: "",
        });  
    });
    test("calls onSubmit function when form is submitted", () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Login onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("login-form");
        fireEvent.click(button);
        expect(click).toHaveBeenCalled();
    });

    test("handleChange function called", () => {
        const click = jest.fn();
        const { getByTestId } = render(
        <Provider store={store}>
            <BrowserRouter>
                <Login onClick={click()}/>
            </BrowserRouter>
        </Provider>
        );
        const button = getByTestId("login-form");
        fireEvent.change(getByTestId("uname"), { target: { value: 'punitha' } }); // invoke handleChange
        fireEvent.change(getByTestId("password"), { target: { value: 'admin123' } }); // invoke handleChange
        fireEvent.click(button);
        expect(click).toHaveBeenCalled(); // Test if handleSubmit has been called 
        expect(click).toHaveBeenCalledTimes(1)

    });
    test('should display a blank login form', () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>  
                    <Login onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("login-form");
        fireEvent.click(button);
        expect(button).toHaveFormValues({
            identifier: "",
            password: "",
        });  
    });
})

  