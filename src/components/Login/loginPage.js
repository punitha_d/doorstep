import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import "./login.css";
import {STRINGS, MESSAGE } from "../../consts";
import {loginUserAction} from "../../actions/actions";

class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    }

  this.handleChange = this.handleChange.bind(this);
  this.onHandleLogin = this.onHandleLogin.bind(this);
  };
  handleChange=(e)=> {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  onHandleLogin = (e) => {
    e.preventDefault();
    if (this.validateForm()) {
      let fields = {};
      fields["identifier"] = "";
      fields["password"] = "";
      this.setState({fields:fields});
    }
    let identifier = e.target.identifier.value;
    let password = e.target.password.value;
    const data = {
      identifier, password
    };

    if( identifier !== "" && password !== ""){
      this.props.dispatch(loginUserAction(data));
  }
  }

  componentDidMount() {
    document.title = 'Sign In';
  }
  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["identifier"]) {
      formIsValid = false;
      errors["identifier"] = MESSAGE.EMPTY_FIELD;
    }
    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = MESSAGE.EMPTY_FIELD;
    } 
    this.setState({
      errors: errors
    });
    return formIsValid;
  }
  render() {
    let login = this.props.response.login.loggedIn;
    let errmsg =this.props.response.login.errresponse;
    return (
      <div className="loginmain"><br /><br />
      <div className="login-form">
      <div className="form-heading">{STRINGS.HEADER.LOGIN}</div>
      {!login ? null  : window.location.href = "/Dashboard"}
      {errmsg?(<div className="errorMsg">{STRINGS.LOGIN.LOGINERROR}</div>):null}
      <div className="cart-message">{this.state.success}</div>
        <div className="login">
        <form onSubmit={this.onHandleLogin} data-testid="login-form">
            <div className="form-label">
            {STRINGS.FORM.USERNAME}
            </div>
            <div className="form-group">
              <input type="text" className="form-control"  name="identifier" placeholder="Enter Username" onChange={this.handleChange} data-testid="uname"/>
              <div className="errorMsg">{this.state.errors.identifier}</div>
            </div>
            <div className="form-label">
            {STRINGS.FORM.PASSWORD}
            </div>
            <div className="form-group">
              <input type="password" className="form-control" data-testid="password" name="password" placeholder="Enter password" onChange={this.handleChange} />
              <div className="errorMsg">{this.state.errors.password}</div>
            </div>      
            <br />       
            <button type="submit" className="submitbutton" id="loginForm" data-testid="button" >{STRINGS.HEADER.LOGIN}</button>
          </form>
          <div className="accountexist">Don't have account? <Link to='register'>Register here</Link></div>
        </div>
      </div>
    </div>
    );
  }
}
const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(LoginPage);