import configureStore from 'redux-mock-store'
import { render, fireEvent,cleanup } from '@testing-library/react'

import '@testing-library/jest-dom/extend-expect'
import renderer from 'react-test-renderer'
import Register from '../registerPage'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
const mockStore = configureStore();
let store;

const initialState = {}; 
store = mockStore(initialState)
describe("Signup component", ()=>{
  afterEach(cleanup);

  test("Renders without creashing", () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
        <Register />
        </BrowserRouter>
      </Provider>
    );
  });
  test("creates Snapshot", () => {
    let snapshot = renderer.create(
            <Provider store={store}>
              <BrowserRouter>
                <Register onSubmit = {jest.fn}/>     
              </BrowserRouter>
            </Provider>
                ).toJSON();
    expect(snapshot).toMatchSnapshot();
  });


  test("OnSubmit Button should mount", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Register />
        </BrowserRouter>
      </Provider>
    );
    const button = getByTestId("button");
    expect(button).toBeInTheDocument();
    expect(button).toHaveClass("submitbutton");
  });
  test('should display a blank Signup form', () => {
    const click = jest.fn();
    const { getByTestId } = render(
        <Provider store={store}>
            <BrowserRouter>  
                <Register onClick={click()}/>
            </BrowserRouter>
        </Provider>
    );
    const button = getByTestId("form");
    fireEvent.click(button);
     // expect(click).toHaveBeenCalledTimes(1);
    expect(button).toHaveFormValues({
      firstname: "",
      lastname: "",
      city: "",
      state: "",
      country: "",
      username: "",
      password: "",
      email: ""
    });  
  });
  test("calls onSubmit function when form is submitted", () => {
    const click = jest.fn();
    const { getByTestId } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Register onClick={click()}/>
        </BrowserRouter>
      </Provider>
    );
    const button = getByTestId("button");
    fireEvent.click(button);
    expect(click).toHaveBeenCalled();
  });
  test("handleChange function called", () => {
    const click = jest.fn();
    const { getByTestId } = render(
    <Provider store={store}>
        <BrowserRouter>
            <Register onClick={click()}/>
        </BrowserRouter>
    </Provider>
    );
    const button = getByTestId("form");
    fireEvent.change(getByTestId("firstname"), { target: { value: 'punitha' } }); // invoke handleChange

    fireEvent.change(getByTestId("username"), { target: { value: 'punitha' } }); // invoke handleChange
    fireEvent.change(getByTestId("password"), { target: { value: 'admin123' } }); // invoke handleChange
    fireEvent.change(getByTestId("email"), { target: { value: 'punitha.pgp' } }); // invoke handleChange
    fireEvent.click(button);
    expect(click).toHaveBeenCalled(); // Test if handleSubmit has been called 
    expect(click).toHaveBeenCalledTimes(1)
  });
  

});