import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUserAction } from '../../actions/actions';
import "./register.css";
import {STRINGS,MESSAGE } from "../../consts";

class RegisterPage extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    }

    this.handleChange = this.handleChange.bind(this);
    this.onHandleRegistration = this.onHandleRegistration.bind(this);
  };
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }
  onHandleRegistration = (e) => {
    e.preventDefault();
    if (this.validateForm()) {
      let fields = {};
      fields["firstname"] = "";
      fields["lastname"] = "";
      fields["city"] = "";
      fields["state"] = "";
      fields["country"] = "";
      fields["username"] = "";
      fields["email"] = "";
      fields["password"] = "";
      this.setState({fields:fields});
      
    }

    let firstname = e.target.firstname.value;
    let lastname = e.target.lastname.value;
    let username = e.target.username.value;
    let email = e.target.email.value;
    let password = e.target.password.value;
    let city = e.target.city.value;
    let state = e.target.state.value;
    let country = e.target.country.value;

    const data = {
      firstname, lastname, username, email, password, city, state, country
    };

    this.props.dispatch(registerUserAction(data));
    alert("Registered Successfully")
  }

  componentDidMount() {
    document.title = 'Sign Up';
  }
  validateForm() {
   // var filter = REGEX.EMAIL;
    
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["firstname"]) {
      formIsValid = false;
      errors["firstname"] =  MESSAGE.EMPTY_FIELD;
    }
    if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = MESSAGE.EMPTY_FIELD;
    }
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = MESSAGE.EMPTY_FIELD;
    }
      // if (fields["email"]) {
      //   if (!filter.test(fields["email"])) {
      //     formIsValid = false;
      //     errors["email"] = MESSAGE.VALID_EMAIL;
      //   }
      // } 
    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = MESSAGE.EMPTY_FIELD;
    } 

    this.setState({
      errors: errors
    });
    return formIsValid;
  }
  render() {  
    return (
      <div className="registermain">
        <div className="register-form">
          <p className="form-heading">{STRINGS.HEADER.REGISTER}</p>
          <div className="register">
          <form data-testid="form" onSubmit={this.onHandleRegistration}>
            <div className="form-label">
            {STRINGS.FORM.FIRSTNAME}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="firstname" id="firstname" data-testid="firstname" onChange={this.handleChange}/>
              <div className="errorMsg">{this.state.errors.firstname}</div>

            </div>
            <div className="form-label">
            {STRINGS.FORM.LASTNAME}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="lastname" id="lastname"  data-testid="lastname" />
            </div>
            <div className="form-label">
            {STRINGS.FORM.CITY}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="city" id="city"  data-testid="city"/>
            </div>
            <div className="form-label">
            {STRINGS.FORM.STATE}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="state" id="state"  data-testid="state" />
            </div>
            <div className="form-label">
            {STRINGS.FORM.COUNTRY}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="country" id="country"  data-testid="country"/>
            </div>
            <div className="form-label"> <label htmlFor="username">
            {STRINGS.FORM.USERNAME}</label>
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="username" id="username"  data-testid="username" onChange={this.handleChange}/>
              <div className="errorMsg">{this.state.errors.username}</div>

            </div>
            <div className="form-label">
            {STRINGS.FORM.EMAIL}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" aria-describedby="emailHelp" name="email" id="email"  data-testid="email" onChange={this.handleChange}/>
              <div className="errorMsg">{this.state.errors.email}</div>

            </div>
            <div className="form-label">
            {STRINGS.FORM.PASSWORD}
            </div>
            <div className="form-group">
            <input type="password" className="form-control" name="password" id="password" data-testid="password" onChange={this.handleChange}/>
            <div className="errorMsg">{this.state.errors.password}</div>

            </div>
            <br />
            <button type="submit" className="submitbutton" data-testid="button" >{STRINGS.HEADER.REGISTER}</button>
          </form>
          <div className="accountexist">Already have account? <Link to='login'>Login here</Link></div>   
        </div>
      </div>
    </div>     
    )
  }
}

const mapStateToProps = (response) => ({
  response
});

export default connect(mapStateToProps)(RegisterPage);
