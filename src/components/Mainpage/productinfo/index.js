import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as types from '../../../actions/types';
import "./product.css";
import {STRINGS } from "../../../consts";
import parse from 'html-react-parser'
import config from '../../../config'


class ProductList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders:1,
      message:''
    } 
    this.addTocart = this.addTocart.bind(this);
  }

IncrementItem = () => {
  this.setState({ orders: this.state.orders + 1 });
}

DecreaseItem = () => {
  this.setState({ orders: this.state.orders - 1 });
}
successHandler=(response)=>{
  if(response && response.length > 0){
    this.setState({ orderupdate:parseInt(response.map(item => item.orderquantity)) });
  }
}
componentDidMount=(props)=>{ 
  document.title="Product details ";
  const {dispatch} = this.props;
  dispatch({ type: types.PRODUCTSINFO_LIST });
  const res = JSON.parse(localStorage.getItem('user'));
  if(res){
    dispatch({ type: types.ORDER_EXIST, onSuccess:this.successHandler});
  }
}
updateItem = (value) => { 
  let prodid;
  const {dispatch} = this.props;
  const itemincart = this.props.orderExist;
  prodid = parseInt(itemincart.map(item => item.id));
  this.setState({ orderupdate: parseInt(this.state.orderupdate) + value })
  let price = this.price.value;
  let orderquantity = parseInt(this.state.orderupdate) + value; 
  let total = orderquantity * price;
  dispatch({ type: types.CART_UPDATE, prodid, orderquantity, total});
  dispatch({ type: types.CART_LIST});
  setTimeout(function() {
    dispatch({type: types.CART_UPDATE_NOTIFICATION})
  }, 3000)
}
  
handleChange(e) {
  this.setState({ [e.target.name]: e.target.value });
}
getdiv(){
  const {dispatch} = this.props;
  setTimeout(function() {
    dispatch({type: types.ADDTOCART_LIST_NOTIFICATION})
  }, 3000)
}
successHandlerAddtocart=()=>{
  const {dispatch} = this.props;
  dispatch({ type: types.ORDER_EXIST, onSuccess:this.successHandler});
}
addTocart=(e)=>{ 
  e.preventDefault();
  const userdetails = JSON.parse(localStorage.getItem('user'))
  let productid = this.id.value;
  let productname = this.name.value;
  let price = this.price.value;
  let orderquantity = this.orderquantity.value;
  let total = orderquantity * price;
  let status ="Incart";
  if(userdetails){
    let userid = userdetails.data.user.id ;
    //dispatching the ADDTOCART Action
    const {dispatch} = this.props;
    dispatch({ type: types.ADDTOCART_LIST, onSuccess:this.successHandlerAddtocart, productid, productname, price, userid, orderquantity, total, status});
    dispatch({ type: types.BASKET_LIST });     
    this.getdiv()
  }else{
    this.setState({
      bodyText: "Login to add items in your cart"
  });
  }
}
render() {

  let order_quantity;
  let itemincart;
  let cartupdate;
  const message = this.props.addTocart
  // const cartupdate = this.props.cartUpdate;
  const res = JSON.parse(localStorage.getItem('user'));

    itemincart = this.props.orderExist;
    if(itemincart && itemincart.length > 0){
      order_quantity = this.state.orderupdate;
    }else{
      order_quantity = this.state.orders;
    }
return (
  <div className="productsmain">
    {this.props.productInfo && this.props.productInfo.map((product) =>
      <div className="pro-info">
        <div key={product.id} className="content-info"> 
          <input type="hidden" name="productid" value={product.id} ref={(input) => { this.id = input }} />
          <input type="hidden" name="productname" value={product.Name} ref={(input) => { this.name = input }} />
          <input type="hidden" name="price" value={product.Price} ref={(input) => { this.price = input }} />
          <div className="product-img">
            <img src= {`${config.BASE_URL}${product.image.formats.thumbnail.url}`} alt ={product.Name} className="prod-img" />
          </div>
          <div className="prod-details">
            <strong>{product.Name}</strong><br />
            <strong>Price:</strong>&nbsp;&nbsp;Rs.{product.Price}&nbsp;&nbsp;<span className="tax">per {product.quantity.Name}&nbsp;&nbsp;{STRINGS.PRODUCT.TAX}</span><br /><br />
            {message === true?(<p className="success-message">{product.Name}&nbsp;&nbsp;{STRINGS.SUCCESS.CART_MSG}</p>):null}
            {cartupdate === true?(<p className="success-message">{STRINGS.SUCCESS.CART_UPDATE_MSG}</p>):null}
            {( itemincart.length >= 1)?(<div className="quantity">
              <strong>Quantity:</strong>&nbsp;&nbsp;
              {(order_quantity > 0)?(
              <button onClick = {()=>{this.updateItem(-1)}}>-</button>):null}&nbsp;&nbsp;
              <input type="text" name = "orderquantity" id = "orderquantity" value={order_quantity} className="quantitysize"/>&nbsp;&nbsp;
              <button onClick = {()=>{this.updateItem(1)}}>+</button>
            </div>):null} <br />           
            {(itemincart.length === 0)?(<div><input type="text" name = "orderquantity" id = "orderquantity" defaultValue="1"  ref={(input) => { this.orderquantity = input }} className="quantitysize"/>&nbsp;&nbsp;
              <button onClick={this.addTocart} className="addtocartbtn">Add to cart</button><div className="cart-message">{this.state.bodyText}</div></div> ):null}
          </div>
        </div>
        <div className="prod-description"><br />{parse(product.description)}</div>
      </div>
    )}
  </div>
 )
}
}
const mapStateToProps = (state) => {

  return {
    productInfo:state.productInfo.response,
    addTocart:state.addTocart.success,
    orderExist:state.orderExist,
    cartUpdate:state.cartUpdate.success
  };
};
export default connect(mapStateToProps)(ProductList);
