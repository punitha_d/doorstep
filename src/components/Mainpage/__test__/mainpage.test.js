import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';
import { BrowserRouter } from 'react-router-dom';

import Aboutus from '../aboutus';
import Contactus from '../contactus'
import Faqs from '../faqs'
import News from '../news'
import Terms from '../terms'
import Privacy from '../privacy'
let store;
const mockStore = configureStore();

const initialState = {}; 
store = mockStore(initialState)
 
describe('Render Component with out crash', () => {
 
    test("creates aboutus Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Aboutus onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
 
    test("creates contactus Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Contactus onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
    test("creates faqs Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Faqs onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
    test("creates news Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <News onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
    test("creates terms Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Terms onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });
    test("creates privacy & policy Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Privacy onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });

});