import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as types from "../../actions/types";
import parse from 'html-react-parser'
import Leftpanel from './leftpanel';
import "./main.css";
class Aboutus extends Component {

  //Rendering about us content
  componentDidMount = () =>{
    const { dispatch} = this.props;
    dispatch({type: types.ABOUTUS});
  } 
  render() {
    return (
      <div className="contactpage">     
        <div className="leftside">
        <Leftpanel />
        </div>
        <div className="page-content">
         {(this.props.aboutus.response || []).filter(res =>   
          res.title === "About Doorstep").map(page => (
            <div className="heading">
              <h2>{page.title}</h2>
              <div className="description" >{parse(page.description)}</div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) =>({aboutus: state.aboutus});
export default connect(mapStateToProps)(Aboutus);
