import React, { Component } from 'react';
import {connect} from 'react-redux';
import Leftpanel from './leftpanel';
import * as types from "../../actions/types";
import parse from 'html-react-parser'
import "./main.css";
class Aboutus extends Component {

  //Rendering about us content
  componentDidMount = () =>{
    const { dispatch} = this.props;
    dispatch({type: types.ABOUTUS});
  } 
  render() {
    return (
      <div className="contactpage">
        <div className="leftside">
        <Leftpanel />
        </div>
        <div className="page-content">
          {(this.props.aboutus.response || []).filter(res =>   
            res.title === "Terms and Conditions").map(term => (
              <div className="heading">
                <h2>{term.title}</h2>
                
                <div className="description" >{parse(term.description)}</div>
              </div>
          ))}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) =>({aboutus: state.aboutus});
export default connect(mapStateToProps)(Aboutus);
