import React, { Component } from 'react';
import {Link } from "react-router-dom";
import STRINGS from "../../consts/strings";
import "./main.css";
class Leftpanel extends Component {
  render() {
    return (
        <div className="sidemenu">
          <ul>
            <li><Link to="aboutus" className="leftlink">{STRINGS.FOOTER.ABOUTUS}</Link></li>
            <li><Link to="/privacy" className="leftlink">{STRINGS.FOOTER.PRIVACY}</Link></li>
            <li><Link to="/terms" className="leftlink">{STRINGS.FOOTER.TERMS}</Link></li>
            <li><Link to="/news" className="leftlink">{STRINGS.FOOTER.NEWS}</Link></li>
            <li><Link to="/faqs" className="leftlink">{STRINGS.FOOTER.FAQ}</Link></li>
            <li><Link to="/contactus" className="leftlink">{STRINGS.FOOTER.CONTACTUS}</Link></li>
          </ul>
         </div>
    )
  }
}
export default Leftpanel;
