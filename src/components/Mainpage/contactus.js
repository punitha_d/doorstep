import React, { Component } from 'react';
import {connect} from 'react-redux';
import Leftpanel from './leftpanel';
import * as types from "../../actions/types";
import parse from 'html-react-parser'
import "./main.css";
import { STRINGS } from '../../consts';

class Contactus extends Component {

  //Rendering about us content
  componentDidMount = () =>{
    const { dispatch} = this.props;
    dispatch({type: types.CONTACTUS});
  } 
  render() {
    return (
      <div className="contactpage">
        <div className="leftside">
          <Leftpanel />
        </div>
        <div className="page-content">
          <h2>{STRINGS.PAGES.CONTACTUS}</h2>
            {this.props.contactus.response&& this.props.contactus.response.map((contact) =>
              <div className="heading" key={contact.id}>
                <h5>{contact.city}</h5>
                <div className="address" >{parse(contact.address)}</div>
              </div>
            )}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) =>({contactus: state.contactus});
export default connect(mapStateToProps)(Contactus);
