import React, { Component } from 'react';
import {connect} from 'react-redux';
import Leftpanel from './leftpanel';
import * as types from "../../actions/types";
import parse from 'html-react-parser'
import "./main.css";
import { STRINGS } from '../../consts';
class Faqs extends Component {

  //Rendering about us content
  componentDidMount = () =>{
    const { dispatch} = this.props;
    dispatch({type: types.FAQS});
  } 
  render() {
    return (
      <div className="contactuspage">
        <div className="leftside">
        <Leftpanel />
        </div>
        <div className="page-content"><h2>{STRINGS.PAGES.FAQS}</h2>
          {this.props.faqs.response && this.props.faqs.response.map((faq) =>
            <div className="heading">
              <h5>{faq.question}</h5>
              <div className="description" >{parse(faq.answer)}</div>
            </div>
          )}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) =>({faqs: state.faqs});
export default connect(mapStateToProps)(Faqs);
