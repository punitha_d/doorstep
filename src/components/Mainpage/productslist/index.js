import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as types from '../../../actions/types';
import { Link } from 'react-router-dom';
import config from '../../../config'

import "./products.css";


class ProductList extends Component {
  componentDidMount=()=>{ 
    this.getProduct();
  }
  getProduct =()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.PRODUCTS_LIST });

  }

  render() {
    
    if((this.props.productlist)=== 0 ){
      return (
        <div className="noproducts">
          No products
        </div>
      )
    }else{
      return (
        <div className="products-main">
          <div className="product-parent">
          {this.props.productlist && this.props.productlist.map((product) =>
            <Link to={`/products-info?id=${product.id}`} className="text-style" >
              <div class="product-child" key={product.id}>
                <img src= {`${config.BASE_URL}${product.image.formats.thumbnail.url}`} alt ={product.Name} className="prod-img" /><br />
                <div className="product-name">{product.Name}<br />Price:&nbsp;&nbsp;{product.Price}&nbsp;&nbsp;per&nbsp;&nbsp;</div>
              </div>
            </Link>
          )}
          </div>
        </div> 
      )
    }
  }
}
const mapStateToProps = (state) => {
    return {
      productlist:state.productsList.response
    };
  };
export default connect(mapStateToProps)(ProductList);
