import React, { Component } from "react";
import {connect} from 'react-redux';
import STRINGS from "../../../consts/strings";
import * as types from '../../../actions/types';
import "./cities.css";

class AvailableCities extends Component {


    componentDidMount=()=>{
      const {dispatch} = this.props;
      dispatch({ type: types.CITIES_LIST });
    }
  render() {
    return (
        <div className="cities">
         <div className="citytitle">{STRINGS.FOOTER.CITIESWESERVE}</div>
         <div className="">
         {this.props.cities.length && this.props.cities.map((city) =>
              <div key={city.id} className="cityname">{city.name},<br /></div>
            )}
         </div>
      </div>
      
    );    
  }
}

const mapStateToProps = (state) => ({ cities: state.cities })

export default connect(mapStateToProps)(AvailableCities);
