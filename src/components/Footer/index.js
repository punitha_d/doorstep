import React, { Component } from "react";
import {Link } from "react-router-dom";
import STRINGS from "../../consts/strings";
import AvailableCities from "./Availablecities";
import Brands from "./Brands";
import Popularcategories from "./Popularcategories";
import downloadapp from '../../assets/downloadapp.png';
import socialicon from '../../assets/socialicon.png';
import "./footer.css";

class FooterMenu extends Component {

  render() {
    return (
      <div className="footer">
        <div className="row-footer">
          <div className="col-4">
            <ul>
              <li className="menu-heading">{STRINGS.FOOTER.FOOTERONE}</li>
              <li><Link to="aboutus" className="footerlink">{STRINGS.FOOTER.ABOUTUS}</Link></li>
              <li><Link to="/privacy" className="footerlink">{STRINGS.FOOTER.PRIVACY}</Link></li>
              <li><Link to="/terms" className="footerlink">{STRINGS.FOOTER.TERMS}</Link></li>
              <li><Link to="/news" className="footerlink">{STRINGS.FOOTER.NEWS}</Link></li>
            </ul>
          </div>
          <div className="col-4">
            <ul>
              <li className="menu-heading">{STRINGS.FOOTER.HELP}</li>
              <li><Link to="/faqs" className="footerlink">{STRINGS.FOOTER.FAQ}</Link></li>
              <li><Link to="/contactus" className="footerlink">{STRINGS.FOOTER.CONTACTUS}</Link></li>
            </ul>
          </div>
          <div className="col-4">
            <ul>
              <li className="menu-heading">{STRINGS.FOOTER.DOWNLOADAPP}</li>
              <li><img src={downloadapp} alt="Logo" width="150" height="116"/></li>
            </ul>
          </div>
          <div className="col-4">
            <ul>
              <li className="menu-heading">{STRINGS.FOOTER.SOCIAL}</li>
              <li><img src={socialicon} alt="Logo" width="217" height="50"/></li>

            </ul>
          </div>
        </div>

          <AvailableCities /> 
          <Brands /> 
          <Popularcategories /> 
          <div className="payment methods">
            {STRINGS.FOOTER.PAYMENT}
          </div>
        <div className="copyright">
          {STRINGS.FOOTER.COPYRIGHT_TEXT}
        </div> 
      </div>  
    );    
  }
}

export default FooterMenu;
