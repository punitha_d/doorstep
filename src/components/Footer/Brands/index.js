import React, { Component } from "react";
import {connect} from 'react-redux';
import STRINGS from "../../../consts/strings";
import * as types from '../../../actions/types';
import "../footer.css";

class Brands extends Component {


    componentDidMount=()=>{
      const {dispatch} = this.props;
      dispatch({ type: types.BRANDS_LIST });
    }
  render() {
    return (
      <div className="popular-items">
        <div className="popular-title">{STRINGS.FOOTER.BRANDS}</div>
        {(this.props.brands || []).filter(res => 
          res.popularbrands === true).map(brand => (
            (<div key={brand.id} className="popularname">{brand.brandname}, <br /></div>)
        ))} 
      </div>
    );    
  }
}

const mapStateToProps = (state) => ({ brands: state.brands })

export default connect(mapStateToProps)(Brands);
