import React, { Component } from "react";
import {connect} from 'react-redux';
import STRINGS from "../../../consts/strings";
import * as types from '../../../actions/types';
import "../footer.css";

class Popularcategories extends Component {

  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.CATEGORIES_LIST });
  }
  render() {
    return (
      <div className="popular-items">
        <div className="popular-title">{STRINGS.FOOTER.CATEGORY}</div>
          {(this.props.categories.response || []).filter(res => 
            res.popularcategory === true).map(category => (
              (<div key={category.id} className="popularname">{category.name}, <br /></div>)
          ))} 
      </div>
    );    
  }
}

const mapStateToProps = (state) => ({ categories: state.categories })

export default connect(mapStateToProps)(Popularcategories);
