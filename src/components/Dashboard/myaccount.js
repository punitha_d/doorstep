import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS } from "../../consts";
import UserMenu from "./userMenu";
import * as types from '../../actions/types';
import { Redirect } from 'react-router-dom';

import "./dashboard.css";

class Myaccount extends Component {
  render() {
    const res = JSON.parse(localStorage.getItem('user')) 
    return (
      <div className="dashboardmain">
   {res?(
     <div className="main" >
        <div className="userleftmenu"> <UserMenu /></div>
          <div className="userrightmenu">
            <div className="dashboard-title">{STRINGS.DASHBOARD.ACCOUNTS} </div>
            <div className="user-details">First Name: {res.data.user.firstname}</div>
            <div className="user-details">Last Name: {res.data.user.lastname}</div>
            <div className="user-details">Email Id: {res.data.user.email}</div>
            <div className="user-details">Mobile No: {res.data.user.mobileno}</div>
            <div className="user-details">Street: {res.data.user.street}</div>
            <div className="user-details">City: {res.data.user.city}</div>
            <div className="user-details">State: {res.data.user.state}</div>
            <div className="user-details" data-testid="myaccount">Country: {res.data.user.country}</div>  
          </div> 
          </div> 
    ): <Redirect to="/login" />} 
      </div>
    );    
  }
}
const mapStateToProps = (response) => (response);
export default connect(mapStateToProps)(Myaccount);
