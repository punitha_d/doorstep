import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS } from "../../consts";
import UserMenu from "./userMenu";
import * as types from '../../actions/types';
import { Link, Redirect } from 'react-router-dom';
import deleteimg from "../../assets/delete.png";

import "./dashboard.css";

class Cartlist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      quantity: 0
    }

  }
  updateItem = (value, order_quantity, productid, price1) => { 
    let prodid;
    prodid = productid;
    let id = productid;
    const {dispatch} = this.props;
    this.setState({ orderupdate: parseInt(order_quantity) + value })
    let price = price1;
    let orderquantity = parseInt(order_quantity) + value; 
    if(orderquantity == 0){
      dispatch({ type: types.CARTDELETE_LIST, id });
      this.getdiv();
    }else{
      let total = orderquantity * price;
      dispatch({ type: types.CART_UPDATE, prodid, orderquantity, total});
      dispatch({ type: types.CART_LIST});
      setTimeout(function() {
        dispatch({type: types.CART_UPDATE_NOTIFICATION})
      }, 3000)
    }
  }

componentDidMount=()=>{

  let userdetail= JSON.parse(localStorage.getItem('user'));
  if(userdetail){
    let userid =   userdetail.data.user.id;
    const {dispatch} = this.props;
    dispatch({ type: types.CART_LIST, userid });
  }
}
deleteCartAction(id, e){
  const {dispatch} = this.props;
  dispatch({ type: types.CARTDELETE_LIST, id });
  dispatch({ type: types.BASKET_LIST });
  let userdetail= JSON.parse(localStorage.getItem('user'));
  let userid =   userdetail.data.user.id;
  dispatch({ type: types.CART_LIST, userid });
  this.getdiv();
} 
getdiv(){
  const {dispatch} = this.props;
  setTimeout(function() {
   dispatch({type: types.REMOVE_CART_NOTIFICATION})
  }, 3000)
}

getSum(total, num) {
  return total + Math.round(num);
}
 render() {
  let total;
  let grandtotal;
  let noofitems;
  let res = JSON.parse(localStorage.getItem('user')) || this.props.res
  const deletemsg = this.props.deleteCart.deleted;  
  const cart = this.props.cartList.response;
  const message = this.props.cartUpdate.success;
  if(cart && cart.length > 0){
    noofitems = cart.length;
    total = cart.map(item => item.total);
    grandtotal = total.reduce(this.getSum, 0);
  }
  return (
        <div className="cartmain">
        {res?(
        <div className="cartmain">
          <input type="hidden" name="userid" value={res.data.user.id} ref={(input) => { this.userid = input }}/>
          <div className="userleftmenu"> <UserMenu /></div>
          <div className="userrightmenu">
            <div className="dashboard-title">{STRINGS.DASHBOARD.CARTLIST}</div>
            {deletemsg === true?(<div className="cellsize success-message">{STRINGS.SUCCESS.CART_DELETE}</div>):null}
            {message === true?(<div className="cellsize success-message">{STRINGS.SUCCESS.CART_UPDATE_MSG}</div>):null}

            <div className="Table">
              <div className="Heading">
                  <div className="Cell">Product Name</div>
                  <div className="Cell"><p>Quantity</p></div>
                  <div className="Cell"><p>Price</p></div>
                  <div className="Cell"><p>Total</p></div>
                  <div className="Cell"></div>
              </div>
              {(this.props.cartList.response || []).filter(res => res.status === "Incart").map(items => ((
                  <div className="Row" key={items.id} >
                    <div className="Cell"><Link to={`/products-info?id=${items.productid}`} className="text-style">{items.productname}</Link></div>
                    <div className="Cell">
                    {(items.orderquantity > 0)?(
                      <button onClick={this.updateItem.bind(this, -1, items.orderquantity, items.id, items.price)}>-</button>):null}
                      <input className="orderquantity" value= {items.orderquantity}/>
                      <button onClick={this.updateItem.bind(this, 1, items.orderquantity, items.id, items.price)}>+</button>
                    </div>
                    <div className="Cell"><p>{items.price}</p></div>
                    <div className="Cell"><p>{items.total}</p></div>
                    <div className="Cell" onClick={this.deleteCartAction.bind(this, items.id, )}><div className="pointer"><img src={deleteimg} alt="basket" width="15" height="15" /></div></div>
                  </div> 
              )))}    
              <br />
              {noofitems > 0?(
                <div>
                  <div className="total">
                    Total no.of.items : {noofitems}<br /><br />
                    Total :{grandtotal}<br />
                  </div>
                  <div className="checkout">
                    <Link to="/checkout" className="text-style" >&nbsp;&nbsp;<button className="checkoutbtn">{STRINGS.CHECKOUT.CHECKOUT}</button></Link>
                  </div>
                </div>
              ):(<div className="delete-message">{STRINGS.CHECKOUT.EMPTY}</div>)}
            </div>
          </div>
        </div>
        ):<Redirect to="/login" />}
      </div>
    );
   }
}
const mapStateToProps = (response) => (response);
export default connect(mapStateToProps)(Cartlist);
