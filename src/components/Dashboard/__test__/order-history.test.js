import configureStore from 'redux-mock-store'
import { render, fireEvent, cleanup } from '@testing-library/react'
import renderer from 'react-test-renderer'
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Orderhistory from '../order-history';
 
let store;
const mockStore = configureStore();
//const initialState = {data:{user:{id: 42, firstname:"punitha", lastname:"d",city:"usercity",state:"userstate",country:"usercountry",username: "punitha",password:"password", email: "punithawe.d@ymedialabs.com", provider: "local", confirmed: false}}}; 
store = mockStore()
const res = {
    data:{
        user:{"id":42,"firstname":"fname","lastname":"lname","city":"lcity","state":"lstate","country":"lcountry"}}
}
describe('Orderhistory Component', () => {
          
    test("renders without crash", () => {
        const { getByTestId } = render(<Provider store={store}>
            <BrowserRouter>
                <Orderhistory />     
            </BrowserRouter>
            </Provider>
        ); 
    });
});