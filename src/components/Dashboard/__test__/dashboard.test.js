import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';
import { BrowserRouter } from 'react-router-dom';
import {render} from '@testing-library/react'
import Dashboardpage from '../dashboard';
import {initialState} from '../../../reducers/loginReducer';

 
let store;
const mockStore = configureStore();

store = mockStore(initialState)
 
describe('Dashboard Component', () => {
 
  test("renders without crash", () => {
    const { getByTestId } = render(<Provider store={store}>
        <BrowserRouter>
            <Dashboardpage />     
        </BrowserRouter>
    </Provider>); });


});