import configureStore from 'redux-mock-store'
import { render, fireEvent, cleanup } from '@testing-library/react'
import renderer from 'react-test-renderer'
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Cartlist from '../cartlist';
 
let store;
const mockStore = configureStore();
//const initialState = {data:{user:{id: 42, firstname:"punitha", lastname:"d",city:"usercity",state:"userstate",country:"usercountry",username: "punitha",password:"password", email: "punithawe.d@ymedialabs.com", provider: "local", confirmed: false}}}; 
const initialState ={}
store = mockStore(initialState)
const res = {
    data:{
        user:{"id":42,"firstname":"fname","lastname":"lname","city":"lcity","state":"lstate","country":"lcountry"}}
}
const deleteCart    = {
    deleteCart:{
        deleted: true,
    },
    cartList:{
        response:[],
    },
}
describe('cartlist Component', () => {
          
    test("renders without crash", () => {
        const { getByTestId } = render(<Provider store={store}>
            <BrowserRouter>
                <Cartlist {...deleteCart} />     
            </BrowserRouter>
            </Provider>
        ); 
    });
    test("creates Snapshot", () => {
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Cartlist {...deleteCart} res={res}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON();
        expect(snapshot).toMatchSnapshot();
    });

});