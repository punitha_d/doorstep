import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS } from "../../consts";
import UserMenu from "./userMenu";
import "./dashboard.css";

class Dashboardpage extends Component {

  componentDidMount() {
    document.title = 'Dashboard';
  }
  render() {
   const res = JSON.parse(localStorage.getItem('user')) 
   return (
      <div className="dashboardmain">
          <div className="main">
            <div className="userleftmenu"> <UserMenu /></div>
            <div className="userrightmenu">{STRINGS.DASHBOARD.WELCOMEMESSAGE}</div>
          </div>
      </div>
    );
  }
}
const mapStateToProps = (response) => ({response});
export default connect(mapStateToProps)(Dashboardpage);
