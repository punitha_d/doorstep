import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS,MESSAGE } from "../../consts";
import UserMenu from "./userMenu";
import * as types from '../../actions/types';
import { Link } from 'react-router-dom';

import "./dashboard.css";

class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    }

    this.handleChange = this.handleChange.bind(this);
    this.onHandleEdit = this.onHandleEdit.bind(this);
  };
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }
  getdiv(){
    const {dispatch} = this.props;
    setTimeout(function() {
     dispatch({type: types.EDIT_PROFILE_NOTIFICATION})
    }, 3000)
  }

  
  onHandleEdit= (e) => {
    e.preventDefault();
    if (this.validateForm()) {
      let fields = {};
      fields["firstname"] = "";
      fields["lastname"] = "";
      fields["city"] = "";
      fields["state"] = "";
      fields["country"] = "";
      fields["mobileno"] = "";
      fields["street"] = "";
      this.setState({fields:fields});
    }
    const res = JSON.parse(localStorage.getItem('user')) 

    let firstname = e.target.firstname.value;
    let lastname = e.target.lastname.value;
    let mobileno = e.target.mobileno.value;
    let street = e.target.street.value;
    let city = e.target.city.value;
    let state = e.target.state.value;
    let country = e.target.country.value;
    let userid = res.data.user.id;
    const {dispatch} = this.props;
    dispatch({ type: types.EDIT_PROFILE,firstname, lastname, mobileno, street, city, state, country,userid});
    this.getdiv();

  }

  componentDidMount() {
    document.title = 'Profile Update';
  }
  validateForm() {    
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (fields["firstname"] === "") {
      formIsValid = false;
      errors["firstname"] =  MESSAGE.EMPTY_FIELD;
    }
    if (fields["mobileno"] === "") {
      formIsValid = false;
      errors["mobileno"] =  MESSAGE.EMPTY_FIELD;
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }
  render() {  

    let message = this.props.response.editProfile.success;
    let  res = JSON.parse(localStorage.getItem('user')) 
    return (
      <div className="editmain">
      {res?(
        <div className="main">
           <div className="userleftmenu"> <UserMenu /></div>
          <div className="userrightmenu">
          <div className="edit-form">
          <p className="form-heading">{STRINGS.DASHBOARD.EDIT}</p>
          {message === true?(<p className="success-message">{STRINGS.SUCCESS.EDIT_MSG}</p>):null}
          <div className="register">
          <form onSubmit={this.onHandleEdit} data-testid="edit-form">
            <div className="form-label">
            {STRINGS.FORM.FIRSTNAME}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="firstname" id="firstname" data-testid="fname" defaultValue={res.data.user.firstname}  onChange={this.handleChange}/>
              <div className="errorMsg">{this.state.errors.firstname}</div>
            </div>
            <div className="form-label">
            {STRINGS.FORM.LASTNAME}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="lastname" id="lastname" defaultValue={res.data.user.lastname} onChange={this.handleChange}/>
            </div>
            <div className="form-label">
            {STRINGS.FORM.EMAIL}
            </div>
            <div className="form-group">
              <input type="text" className="form-control readonly" aria-describedby="emailHelp" name="email" id="email" defaultValue={res.data.user.email} readOnly = {true} />
              <div className="errorMsg">{this.state.errors.email}</div>
            </div>
            <div className="form-label">
            {STRINGS.FORM.MOBILE}
            </div>
            <div className="form-group">
              <input type="number" className="form-control"  name="mobileno" id="mobileno" data-testid="mobile" defaultValue={res.data.user.mobileno} onChange={this.handleChange}/>
              <div className="errorMsg">{this.state.errors.mobileno}</div>
            </div>
            <div className="form-label">
            {STRINGS.FORM.STREET}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="street" id="street" defaultValue={res.data.user.street} onChange={this.handleChange}/>
            </div>
            <div className="form-label">
            {STRINGS.FORM.CITY}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="city" id="city" defaultValue={res.data.user.city} onChange={this.handleChange}/>
            </div>
            <div className="form-label">
            {STRINGS.FORM.STATE}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="state" id="state" defaultValue={res.data.user.state} onChange={this.handleChange}/>
            </div>
            <div className="form-label">
            {STRINGS.FORM.COUNTRY}
            </div>
            <div className="form-group">
              <input type="text" className="form-control" name="country" id="country" defaultValue={res.data.user.country} onChange={this.handleChange}/>
            </div>
           
            <br />
            <div className="form-heading"><button type="submit" className="submitbutton" data-testid="button"> {STRINGS.DASHBOARD.EDIT}</button></div>
          </form>
        </div>
      </div>
          </div>
          </div>
          ): (<div className="logged-in">This page is only for Logged in users.Please <Link to="/login">Login to contine</Link></div>)}
        </div>     
    )
  }
}

const mapStateToProps = (response) => ({
  response
});

export default connect(mapStateToProps)(EditProfile);
