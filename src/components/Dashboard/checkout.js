import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS } from "../../consts";
import UserMenu from "./userMenu";
import * as types from '../../actions/types';
import { Redirect } from 'react-router-dom';
import "./dashboard.css";

class Checkout extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

componentDidMount=()=>{
  const res = JSON.parse(localStorage.getItem('user')) 
  let userid = res.data.user.id;
  const {dispatch} = this.props;
  dispatch({ type: types.USER_PROFILE, userid });
  dispatch({ type: types.CART_LIST });
}
getdiv(){
  const {dispatch} = this.props;
  setTimeout(function() {
   dispatch({type: types.ORDERSTATUS_LIST_NOTIFICATION})
  }, 3000)
}
orderAction(prodid, e){
  const {dispatch} = this.props;
  var arrayLength = prodid.length;
  for (var i = 0; i < arrayLength; i++) { 
    let id =  prodid[i];
    let status = "Ordered"
    dispatch({ type: types.ORDERSTATUS_LIST,id, status });
    dispatch({ type: types.BASKET_LIST });
  }
  this.getdiv()
} 

getSum(total, num) {
  return total + Math.round(num);
}
 render() {
  let total;
  let grandtotal;
  let productid; 
  const res = JSON.parse(localStorage.getItem('user')) 
  const cart = this.props.cartList.response;
  const ordermsg = this.props.orderStatus.orderstatus;
  if(cart && cart.length > 0){
    productid = cart.map(item => item.id);
    total = cart.map(item => item.total);
    grandtotal = total.reduce(this.getSum, 0);
  }

  return (
        <div className="cartmain">
          {res?(
          <div className="cartmain">
          <div className="userleftmenu"> <UserMenu /></div>
          <div className="userrightmenu">
            <div className="delivery-address">
            {!ordermsg ? null : <Redirect to='order-history' />}
            {STRINGS.CHECKOUT.DELIVERY}<br /><br />
              <div className="user-address">
                {res.data.user.username},<br />
                {res.data.user.street},<br />
                {res.data.user.city},<br />
                {res.data.user.state},<br />
                {res.data.user.country},<br />
                {res.data.user.mobileno},<br />
                {res.data.user.email}<br />
              </div>
            </div>
            <br /><br />
            <div className="delivery-address">
            {STRINGS.CHECKOUT.PAYMENT}<br /><br />
              <div className="user-address">
              {STRINGS.CHECKOUT.COD}
              </div><br /><br />
              <div className="total">
              {STRINGS.CHECKOUT.TOTAL} : {grandtotal}
              </div>
            </div>
            <br />
            <div className="order-now"><button className="orderbtn" onClick={this.orderAction.bind(this, productid)}>Order Now</button></div>
          </div>
          </div>
          ):( <Redirect to="/Login" />
          )}
          </div>
      );
   }
}
const mapStateToProps = (response) => (response);
export default connect(mapStateToProps)(Checkout);
