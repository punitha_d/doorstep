import React, { Component } from 'react';
import "./dashboard.css";
import { Link } from 'react-router-dom';
import {STRINGS } from "../../consts";

class UserMenu extends Component {

  render() {
  return (
    <div className="rowmenu">
      <div className="col-1"><Link to="/myaccount" className="user-heading">{STRINGS.DASHBOARD.ACCOUNTS}</Link> </div>
      <div className="col-1"><Link to="/edit-profile" className="user-heading">{STRINGS.DASHBOARD.EDIT}</Link></div>
      <div className="col-1"><Link to="/mycart" className="user-heading">{STRINGS.DASHBOARD.CART}</Link></div>
      <div className="col-1"><Link to="/order-history" className="user-heading">{STRINGS.DASHBOARD.ORDERHISTORY}</Link></div>    
    </div>
  );
    
  }
}
export default UserMenu;
