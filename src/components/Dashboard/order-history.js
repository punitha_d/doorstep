import React, { Component } from 'react';
import { connect } from 'react-redux';
import {STRINGS } from "../../consts";
import UserMenu from "./userMenu";
import * as types from '../../actions/types';
import { Redirect } from 'react-router-dom';

import "./dashboard.css";

class Orderhistory extends Component {
componentDidMount=()=>{
  let userdetail= JSON.parse(localStorage.getItem('user'));
  if(userdetail){
    const {dispatch} = this.props;
    dispatch({ type: types.ORDER_LIST });
  }
}

orderAction(id){
  let status = "Cancelled";
  const {dispatch} = this.props;
  dispatch({ type: types.ORDERSTATUS_LIST,id, status });
  dispatch({ type: types.ORDER_LIST });
}
  render() {
    const res = JSON.parse(localStorage.getItem('user'))
  if(res){
      return (
        <div className="cartmain">
          
          <input type="hidden" name="userid" value={res.data.user.id | null} ref={(input) => { this.userid = input }}/>
          <div className="userleftmenu"> <UserMenu /></div>
          <div className="userrightmenu">
            <div className="dashboard-title">{STRINGS.DASHBOARD.ORDER}</div>
            <div className="Table">
              <div className="Heading">
                  <div className="Cell"><p>Order Id</p></div>
                  <div className="Cell"><p>Product Name</p></div>
                  <div className="Cell"><p>Quantity</p></div>
                  <div className="Cell"><p>Price</p></div>
                  <div className="Cell"><p>Total</p></div>
                  <div className="Cell"><p>Status</p></div>
                  <div className="Cell"></div>
              </div>
              {(this.props.orderList.data || []).filter(res => res.status !== "Incart").map(items => ((
                <div className="Row" key={items.id}>
                  <div className="Cell"><p>{items.id}</p></div>
                  <div className="Cell"><p>{items.productname}</p></div>
                  <div className="Cell"><p>{items.orderquantity}</p></div>
                  <div className="Cell"><p>{items.price}</p></div>
                  <div className="Cell"><p>{items.total}</p></div>
                  <div className={`Cell ${items.status === "Cancelled" ? 'error-message' : 'success-message'}`}>{items.status}</div>
                  <div className="Cell">
                    {items.status !== "Completed" && items.status !== "Cancelled" ?
                     (<button className="cancelbutton" onClick={this.orderAction.bind(this, items.id)} >{STRINGS.DASHBOARD.CANCELORDER}</button>):null}
                  </div>
                </div> 
              )))}            
            </div>
          </div>
        </div>
      );
    }else{
      return (
        <Redirect to="/login" />
      );
    }
   }
}
const mapStateToProps = (response) => (response);
export default connect(mapStateToProps)(Orderhistory);
