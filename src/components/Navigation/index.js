import React, { Component } from "react";
import {connect} from 'react-redux';
import STRINGS from "../../consts/strings";
import {Link } from "react-router-dom";
import "./navigation.css";
import * as types from '../../actions/types';
import banner from '../../assets/banner.png';


class HeaderMenu extends Component {

  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.CATEGORIES_LIST });
  }
  getdetails=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.PRODUCTS_LIST });
  }
  render() {
    return (
      <div className="navigation navi-border">
        <div className="position"><img src={banner} alt="Logo" width="100%"/></div>
        <div className="navi-left">
          <button className="navigationbtn">{STRINGS.HEADER.SHOPBYCATEGORY} <i className="fa fa-caret-down"></i></button>
              <div className="navi-content">
                {this.props.categories && this.props.categories.map((category) =>
                <div key={category.id} onClick={this.getdetails}>
                  <Link to={`/products-list?id=${category.id}`} className="category-name">{category.name}<br /></Link></div>
                )}
              </div>   
         </div> 
      </div> 
    );    
  }
}
const mapStateToProps = (state) => {

  return {
    categories:state.categories.response,
    baskets:state.basketList
  };
};


export default connect(mapStateToProps)(HeaderMenu);
