import React from 'react'
import { Link } from 'react-router-dom';
import "./search.css"
const Suggestions = (props) => {
  const options = props.results.map(list => (
    <li key={list.id} className="prod-list">
       <Link to={`/products-info?id=${list.id}`} className="text-style" >{list.Name}</Link>
       <p className="categ-name">{list.category.name}</p>

    </li>
  ))
  return <ul>{options}</ul>
}

export default Suggestions