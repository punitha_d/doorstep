import React, { Component } from 'react'
import axios from 'axios'
import Suggestions from "./suggestions"
import './search.css'
class Search extends Component {
  state = {
    query: '',
    results: []
  }

  getInfo = () => {
    axios.get(`http://localhost:1338/products?Name=${this.state.query}`)
      .then(({ data }) => {
        this.setState({
          results: data                                      
        })
        console.log(this.state.results)
      })
  }

  handleInputChange = () => {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
          this.getInfo()
      } 
    })
  }

  render() {
    return (
      <form  data-testid="searchform">
        <input
          placeholder="Search for products..."  data-testid="search"
          ref={input => this.search = input}
          onChange={this.handleInputChange}
         className="search-form"/>
         <button type="submit" className="submitbtn" name="search"  data-testid="button"><i className="fa fa-search"></i></button>
        <Suggestions results={this.state.results} />
       </form>
    )
  }
}

export default Search