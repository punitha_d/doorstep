import configureStore from 'redux-mock-store'
import { render, fireEvent, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import renderer from 'react-test-renderer'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import Search from '../search';
 
let store;
const mockStore = configureStore();

const initialState = {}; 
store = mockStore(initialState)
 
describe('Search Component', () => {

    afterEach(cleanup);

    test("creates Snapshot", () => {
        const onSubmit = jest.fn();
        let snapshot = renderer.create(
                <Provider store={store}>
                    <BrowserRouter>
                        <Search onSubmit = {onSubmit}/>     
                    </BrowserRouter>
                </Provider>
            ).toJSON(); 
        expect(snapshot).toMatchSnapshot();
    });
    test("calls onSubmit function when search form is submitted", () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Search onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("searchform");
        fireEvent.click(button);
        expect(click).toHaveBeenCalled();
        expect(click).toHaveBeenCalledTimes(1);
    });
    test("check function should not call more than 1 when submit form", () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Search onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("searchform");
        fireEvent.click(button);
        expect(click).toHaveBeenCalled();
        //expect(click).toHaveBeenCalledTimes(2);
    });
    test('should display a blank search form', () => {
        const click = jest.fn();
        const { getByTestId } = render(
            <Provider store={store}>
                <BrowserRouter>  
                    <Search onClick={click()}/>
                </BrowserRouter>
            </Provider>
        );
        const button = getByTestId("searchform");
        fireEvent.click(button);
        expect(button).toHaveFormValues({
            search: ""
        });  
    });
});