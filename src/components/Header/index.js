import React, {Component} from "react";
import STRINGS from "../../consts/strings";
import { connect } from 'react-redux';
import Search from "../Search/search"
import { Link } from 'react-router-dom';
import * as types from '../../actions/types';
import basketimg from '../../assets/basket.jpeg';

import './header.css';

class HeaderMenu extends Component {
  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.BASKET_LIST });
  }

 logout(e) {
     e.preventDefault();
     localStorage.removeItem('user');
     window.location.href = "/login";
  };   
   render() {

  const res = JSON.parse(localStorage.getItem('user'));
  const basket = this.props.response.basketList;
  return (  
    <div className="header">
        <div className="top-header"><Link to="/">{STRINGS.HEADER.LOGO_TITLE}</Link></div>
        <div className="search-box"><Search /></div>
        {basket > 0?(
            <div className="cart-basket">
              <div className="basket-items">{basket}</div>
              <Link to={`/mycart`} className="basket-name"><img src={basketimg} alt="basket" width="30" height="30"/> </Link>
             </div>
            ):null}            
        {res?(
          <div className="top-links">
             Hi {!res ? null : res.data.user.username},<br /><br />
              <Link to="Myaccount" className="headerlink">{STRINGS.HEADER.MYACCOUNT}</Link>&nbsp;&nbsp;<span className="headerlink">|</span>&nbsp;&nbsp;
              <div onClick={this.logout} className="headerlink">{STRINGS.HEADER.LOGOUT}</div>&nbsp;&nbsp;
          </div>
        ):(
          <div className="top-links">
            <Link to="Login" className="headerlink">{STRINGS.HEADER.LOGIN}</Link>&nbsp;&nbsp;<span className="headerlink">|</span>&nbsp;&nbsp;
            <Link to="Register" className="headerlink">{STRINGS.HEADER.REGISTER}</Link>&nbsp;&nbsp;
          </div>   
        )}
    </div>
    );
  }
}
     

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(HeaderMenu); 