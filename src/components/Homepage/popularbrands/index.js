import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import config from '../../../config'

import * as types from '../../../actions/types';
import "../home.css";

class Popularbrands extends Component {
  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.BRANDS_LIST });
  }
  render() {
    return (
      <div className="row-brand">
         {(this.props.brands || []).filter(res =>   
          res.popularbrands === true).map(brand => (
            (<Link to={`/products-list?brandid=${brand.id}`} key={brand.id} className="col-5"><img src= {`${config.BASE_URL}${brand.brandlogo.formats.thumbnail.url}`} alt={brand.brandname} className="brand-img"/><br /><div className="pbrandname">{brand.brandname}</div></Link>)
        ))} 
      </div>
    )
  }
}

const mapStateToProps = (state) => ({ brands: state.brands })

export default connect(mapStateToProps)(Popularbrands);
