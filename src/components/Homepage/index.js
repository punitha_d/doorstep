import React, { Component } from "react";
import PopularBrands from "./popularbrands";
import Popularcategries from "./popularCategories";
import BankOffer from "./bankOffer";
import STRINGS from "../../consts/strings";
import banner from '../../assets/banner1.png';

import "./home.css"

class Homepage extends Component {

  render() {
    return (
      <div className="homapage">
        <div className="home-heading">{STRINGS.FOOTER.BRANDS}</div>
        <div className="heading-border"></div>
        <div className="divider-space"></div>
        <PopularBrands />
        <div className="home-position"><img src={banner} alt="Logo" width="100%"/></div>

        <div className="home-heading">{STRINGS.FOOTER.CATEGORY}</div>
        <div className="heading-border"></div>
        <div className="divider-space"></div>
        <Popularcategries />

        <div className="home-heading">{STRINGS.HOMEPAGE.BANKOFFER}</div>
        <div className="heading-border"></div>
        <div className="divider-space"></div>
        <BankOffer />
        <div className="divider-space"></div>
      </div>
    );    
  }
}

export default Homepage;
