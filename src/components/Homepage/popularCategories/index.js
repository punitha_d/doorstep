import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import config from '../../../config'
import * as types from '../../../actions/types';
import "../home.css";

class Popularcategries extends Component {
  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.CATEGORIES_LIST });
  }
  render() {
    return (
      <div className="row-brand">
         {(this.props.categories.response || []).filter(res => 
          res.popularcategory === true).map(category => (
            (<Link to={`/products-list?id=${category.id}`} key={category.id} className="col-5"><img src= {`${config.BASE_URL}${category.image.formats.thumbnail.url}`} alt ={category.name}  className="brand-img"/><br /><div className="pbrandname">{category.name}</div></Link>)
        ))} 
      </div>
    )
  }
}
const mapStateToProps = (state) => ({ categories: state.categories })

export default connect(mapStateToProps)(Popularcategries);
