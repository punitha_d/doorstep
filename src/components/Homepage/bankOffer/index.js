import React, { Component } from 'react';
import {connect} from 'react-redux';
import config from '../../../config'
import * as types from '../../../actions/types';
import "../home.css";

class BankOffer extends Component {
  componentDidMount=()=>{
    const {dispatch} = this.props;
    dispatch({ type: types.BANK_OFFER });
  }
  render() {
    return (
      <div className="row-brand">
        {this.props.bankOffer.data && this.props.bankOffer.data.map((bank) =>
            <div key={bank.id} className="col-5"> 
               <img src= {`${config.BASE_URL}${bank.image.formats.thumbnail.url}`} alt ={bank.name} className="prod-img" />
            </div>
          )}        

      </div>
    )
  }
}

const mapStateToProps = (response) => (response)

export default connect(mapStateToProps)(BankOffer);
