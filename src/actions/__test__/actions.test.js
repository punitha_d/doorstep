import {
  loginUserAction,
  registerUserAction, 
  editUserAction, 
  aboutusContent, 
  productList, 
  productInfo,
  cartList,
  orderStatus,
  gettUserAction
} from '../actions';
import * as types from "../types";
import {loginform, registerform, aboutus} from "../mockData"

describe('UT for actions', () => {
    it('should create an action for  LOGIN_USER success', () => {
        const user = loginform;
        const username = user.identifier;
        const password = user.password;
        const loginpage ={
          type: types.LOGIN_USER,
         username,
         password        
        }
        const action = loginUserAction(username, password)
        expect(action).toEqual(loginpage)
    });

    it('should create an action for  REGISTER_USER success', () => {
        const user = registerform;
        const registerpage ={
          type: types.REGISTER_USER,
          user
        }
        const action = registerUserAction(user)
        expect(action).toEqual(registerpage)   
    });
    it('should create an action for  ABOUTUS_SUCCESS success', () => {
        const payload = aboutus;
        const aboutuspage ={
          type: types.ABOUTUS_SUCCESS,
          payload
        }
        const action = aboutusContent(payload)
        expect(action).toEqual(aboutuspage)   
    });
    it('should create an action for  PRODUCTS_LIST success', () => {
        const payload = 23;
        const propage ={
          type: types.PRODUCTS_LIST,
          payload
        }

        const action = productList(payload)
        expect(action).toEqual(propage)   
    });

    it('should create an action for  PRODUCTSINFO_LIST success', () => {
        const payload = 23;
        const propage ={
          type: types.PRODUCTSINFO_LIST,
          payload
        }

        const action = productInfo(payload)
        expect(action).toEqual(propage)   
    });
    it('should create an action for  EDIT_PROFILE success', () => {
      const payload = registerform;
      const editpage ={
        type: types.EDIT_PROFILE,
        payload
      }
      const action = editUserAction(payload)
      expect(action).toEqual(editpage)   
    });
    it('should create an action for USER_PROFILE success', () => {
      const payload = 32;
      const userprofile ={
        type: types.USER_PROFILE,
        payload
      }
      const action = gettUserAction(payload)
      expect(action).toEqual(userprofile)   
    });

    it('should create an action for CART_LIST success', () => {
      const cartlist ={
        type: types.CART_LIST,
      }
      const action = cartList()

     expect(action).toEqual(cartlist)   
    });
    it('should create an action for ORDERSTATUS_LIST success', () => {
      const orderstatuslist ={
        type: types.ORDERSTATUS_LIST,
      }
      const action = orderStatus()
    
     expect(action).toEqual(orderstatuslist)   
    });
});





