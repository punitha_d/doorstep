const loginform = 
  {
    identifier: "test",
    password: "pass"
  };
  
const registerform =
  {
    id:null,
    firstname:"fname",
    lastnme:"lname",
    city:"usercity",
    state:"userstate",
    country:"ucountry",
    email:"punitha.ph@gmail.com",
    identifier: "test",
    password: "pass"
  };
  
 const aboutus = {
  id:null,
  title:"About Doorstep",
  description:"<h4>We make digital products & experiences that have lasting impact.</h4>\n<strong>Dreamers & doers</strong>\n<br><br>\n\nRight from fresh Fruits and Vegetables, Rice and Dals, Spices and Seasonings to Packaged products, Beverages, Personal care products, Meats – we have it all.Choose from a wide range of options in every category, exclusively handpicked to help you find the best quality available at the lowest prices. <br><br>\nSelect a time slot for delivery and your order will be delivered right to your doorstep, anywhere in Bangalore, Hyderabad, Mumbai, Pune, Chennai, Delhi, Noida, Mysore, Coimbatore, Vijayawada-Guntur, Kolkata, Ahmedabad-Gandhinagar, Lucknow-Kanpur, Gurgaon, Vadodara, Visakhapatnam, Surat, Nagpur, Patna, Indore and Chandigarh Tricity You can pay online using your debit / credit card or by cash / sodexo on delivery.<br><br>\nWe guarantee on time delivery, and the best quality!\n\nDid you ever imagine that the freshest of fruits and vegetables, top quality pulses and food grains, dairy products and hundreds of branded items could be handpicked and delivered to your home, all at the click of a button? India’s first comprehensive online megastore, bigbasket.com, brings a whopping 20000+ products with more than 1000 brands, to over 4 million happy customers. From household cleaning products to beauty and makeup, bigbasket has everything you need for your daily needs. bigbasket.com is convenience personified We’ve taken away all the stress associated with shopping for daily essentials, and you can now order all your household products and even buy groceries online without travelling long distances or standing in serpentine queues.<br><br>\n\n Add to this the convenience of finding all your requirements at one single source, along with great savings, and you will realize that bigbasket- India’s largest online supermarket, has revolutionized the way India shops for groceries. Online grocery shopping has never been easier. Need things fresh? Whether it’s fruits and vegetables or dairy and meat, we have this covered as well! Get fresh eggs, meat, fish and more online at your convenience. Hassle-free Home Delivery options",
  published_at:"2021-02-02T15:46:21.000Z",
  created_at:"2021-02-02T15:46:19.000Z",
  updated_at:"2021-02-12T07:08:37.000Z"
 };

const product = [
  {
    id:null,
    Name:"Banana",
   description:"<h4>About the Product</h4><br />\nFresh, tiny small sized, directly procured from the farm, this variety is called Yelakki in Bangalore and Elaichi in Mumbai. Despite its small size, they are naturally flavoured, aromatic and sweeter compared to regular bananas. Yelakki bananas are around 3- 4 inches long, and contain a thinner skin and better shelf life than Robusta bananas.<br />\n\n<h4>Storage and Uses</h4><br />\n\nStore them in a cool, dry place away from direct sunlight. Fresh, raw yelakkis are green. They turn into golden yellow on ripening. Look for brown speckles and yellow skin to identify ripened ones.\nSlice them onto pancakes, blend into smoothies or add to fruit salads. Heat brings out the bananas' creamy sweetness. Try baking or sauteing them with butter and sugar for a delicious dessert.<br />\n\n<h4>Benifits</h4><br />\nOne banana supplies 30 percent of the daily vitamin B6 requirement and is rich in vitamin C and potassium. It reduces appetite and promotes weight loss, while also boosting the immune system and keeping the bones strong. It is very good for pregnant women and athletes.\n<br />",
   brand:null,
   published_at:"2021-01-28T09:56:54.000Z",
   created_at:"2021-01-28T09:56:43.000Z",
   updated_at:"2021-02-12T16:45:01.000Z",
   Price:"70",
   SKU:"SKU-12345",
   quantity:null,
   category:{
   id:null,
   name:"Fruits",
   published_at:"2021-01-21T07:22:32.000Z",
   created_at:"2021-01-21T07:22:28.000Z",
   updated_at:"2021-02-04T19:50:27.000Z",
   product:4,
   popularcategory:null,
   image:null
  },
  image:{
    id:null,
    name:"papaya-fruit.jpg",
    alternativeText:"",
    caption:"",
    width:700,
    height:467,
    formats:{
      thumbnail:{
        name:"thumbnail_papaya-fruit.jpg",
        hash:"thumbnail_papaya_fruit_f4b51b61da",
        ext:".jpg",
        mime:"image/jpeg",
        width:234,
        height:156,
        size:5.07,
        path:null,
        url:"/uploads/thumbnail_papaya_fruit_f4b51b61da.jpg"
      }},
      hash:"papaya_fruit_f4b51b61da",
      ext:".jpg",
      mime:"image/jpeg",
      size:23.01,
      url:"/uploads/papaya_fruit_f4b51b61da.jpg",
      previewUrl:null,
      provider:"local",
      provider_metadata:null,
      created_at:"2021-01-28T09:56:04.000Z",
      updated_at:"2021-01-28T09:56:19.000Z"
    }}];

  const cartlist = {
    id:null,
    published_at:"2021-02-23T12:23:38.000Z",
    created_at:"2021-02-23T12:23:38.000Z",
    updated_at:"2021-02-23T12:23:38.000Z",
    productid:"4",
    productname:"Orange- Fresho Orange - Nagpur, Regular (End Of Season)",
    quantity:null,
    price:"90",
    total:"90",
    userid:"42",
    status:"Incart",
    orderquantity:"1"
  };
// Using CommonJS style export so we can consume via Node (without using Babel-node)
// 
module.exports = {
  loginform,
  registerform,
  product,
  aboutus,
  cartlist
};
