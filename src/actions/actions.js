import * as types from './types';

export const registerUserAction = (user) => {
  return {
    type: types.REGISTER_USER,
    user
  } 
};
export const loginUserAction = (username, password) => {
  return {
    type: types.LOGIN_USER,
    username,
    password,
  }
};

export const getUserAction = (payload)=>{
  return {
    type: types.USER_DETAILS,
    payload
  }
}
//Shop by category list
export const loadCategory = (payload) => {
  return {
    type: types.CATEGORIES_LIST,
    payload
  }
};
//About us page content
export const aboutusContent =(payload) =>{
  return{
    type: types.ABOUTUS_SUCCESS,
    payload
  }
}

//About us page content
export const faqsContent =(payload) =>{
  return{
    type: types.FAQS_SUCCESS,
    payload
  }
}
//CONTACTUS
export const contactusContent =(payload) =>{
  return{
    type: types.CONTACTUS_SUCCESS,
    payload
  }
}

//Footer SECTION 
//Cities we serve list
export const loadCities = (payload) => {
  return {
    type: types.CITIES_LIST,
    payload
  }
};

// Popular Brands list
export const brandsList = (payload) => {
  return {
    type:types.BRANDS_LIST,
    payload
  }
}
//Home page popular categories list
export const popularCategoriesList = (id)=>{
  return{
    type: types.POPULARCATEGORIES_LIST,
    payload: id,
  }
}
//Products list page
export const productList = (payload) =>{
  
  return{
    type: types.PRODUCTS_LIST,
    payload
  }
}
//single product page
export const productInfo = (payload) =>{
  return{
    type: types.PRODUCTSINFO_LIST,
    payload
  }
}

export const addTocart = (payload) =>{
  return{
    type: types.ADDTOCART_LIST,
    payload
  }
}
export const deleteCart = (payload) =>{
  return{
    type: types.CARTDELETE_LIST,
    payload
  }
}
export const basketList = (payload) =>{
  return{
    type: types.BASKET_LIST,
    payload
  }
}

//Bank offer
//Products list page
export const bankOffer = (payload) =>{
  return{
    type: types.BANK_OFFER,
    payload
  }
}

//USER profile
export const gettUserAction = (payload) =>{
  return{
    type: types.USER_PROFILE,
    payload
  }
}

//Edit profile
export const editUserAction = (payload) =>{
  return{
    type: types.EDIT_PROFILE,
    payload
  }
}
//cart list
export const cartList = () =>{
  return{
    type: types.CART_LIST,
  }
}
//UPDATE cart list
export const cartupdate = (payload) =>{
  return{
    type: types.CART_UPDATE,
    payload
  }
}

//ORDER STATUS

export const orderStatus = (payload) =>{
  return{
    type: types.ORDERSTATUS_LIST ,
    payload
  }
}
//ORDER Exist

export const orderExist = () =>{
  return{
    type: types.ORDER_EXIST
    
  }
}