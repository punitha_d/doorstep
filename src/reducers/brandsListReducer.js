import * as types from "../actions/types";
export default function brands(state= [], action){
    let response = action.payload;
    switch(action.type){
        case types.BRANDS_LIST_SUCCESS:
            return (state, response);
        case types.BRANDS_LIST_ERROR:
            return (state, response);
        default:
            return state;
    }
}

