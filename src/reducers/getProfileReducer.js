import * as types from '../actions/types';

export default function getProfile(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.USER_PROFILE_SUCCESS: 
        return { ...state, response};
    case types.USER_PROFILE_ERROR:
      return { ...state, response };
    default:
      return state;
  }
};