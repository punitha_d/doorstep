import * as types from '../actions/types';
export default function cities(state=[], action){
    let response = action.payload;
    switch(action.type){
        case types.CITIES_LIST_SUCCESS:
            return (state, response);
        case types.LOGIN_USER_ERROR:
            return(state, response);
        default:
            return state;
    }
}