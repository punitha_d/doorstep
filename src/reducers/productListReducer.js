import * as types from '../actions/types';

export default function productsList(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.PRODUCTS_LIST_SUCCESS: 
        return { ...state, response};
    case types.PRODUCTS_LIST_ERROR:
      return { ...state, response };
    default:
      return state;
  }
};