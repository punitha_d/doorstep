import * as types from "../actions/types";

export default function orderList(state = [], action){
    let response = action.payload;
    switch(action.type){
        case types.ORDER_LIST_SUCCESS:
            return (state, response); 
        case types.ORDER_LIST_ERROR:
            return (state, response);
        default:
            return state;
    }

}