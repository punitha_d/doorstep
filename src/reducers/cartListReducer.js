import * as types from '../actions/types';

export default function cartList(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.CART_LIST_SUCCESS: 
        return { ...state, response};
    case types.CART_LIST_ERROR:
      return { ...state, response };
    default:
      return state;
  }
};