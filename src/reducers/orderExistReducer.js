import * as types from "../actions/types";

export default function orderExist(state = [], action){
    let response = action.payload;
    switch(action.type){
        case types.ORDER_EXIST_SUCCESS:
            return (state, response); 
        case types.ORDER_EXIST_ERROR:
            return (state, response);
        default:
            return state;
    }

}