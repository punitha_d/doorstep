import faqsReducer from '../faqsReducer';
import {initialState} from '../faqsReducer';

describe("FAQS Reducer", ()=>{
  //Reducers
  // it('should set true for loggedIn for LOGIN_USER_SUCCESS action', () => {
  //     let loginState = loginReducer({"identifier":"punitha","password":"admin123"}, { type: 'LOGIN_USER_SUCCESS' })
  //     expect(loginState.loggedIn).toEqual(true);
  // });

  it('should handle FAQS_SUCCESS', () => {
    expect(
      faqsReducer(initialState,
      {
        type: 'FAQS_SUCCESS'
      })
    )
  })
  it('should handle the Action FAQS_ERROR', () => {
    expect(
      faqsReducer(initialState,
        {
            faq: true,
        },
        {
          type: 'FAQS_ERROR',
        },
      ),
    ).toEqual({
        "faq": false,"error":null, "faqs":[]
    });
  });
});