import registerReducer from '../registerReducer';
import {initialState} from '../registerReducer';

describe("Register Reducer", ()=>{
  //Reducers
  it('should set true for registered', () => {
      let registerState = registerReducer({"firstname":"punitha","email":"punitha.d@ymedialabs.com","identidier":"punitha","password":"admin123"}, { type: 'REGISTER_USER_SUCCESS' })
      expect(registerState.registered).toEqual(true);
  });

  it('should handle REGISTER_USER_SUCCESS', () => {
    expect(
      registerReducer(initialState,
      {
        type: 'REGISTER_USER_SUCCESS'
      })
    ).toMatchSnapshot()
  })
it('should handle the Action  REGISTER_USER_ERROR', () => {
    expect(
      registerReducer(initialState,
        {
            registered: true,
        },
        {
          type: 'REGISTER_USER_ERROR',
        },
      ),
    ).toEqual({
        "registered": false,"error":null, "register":[]
    });
  });
});
