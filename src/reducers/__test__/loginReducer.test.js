import loginReducer from '../loginReducer';
import {initialState} from '../loginReducer';

describe("Login Reducer", ()=>{
  //Reducers
  // it('should set true for loggedIn for LOGIN_USER_SUCCESS action', () => {
  //     let loginState = loginReducer({"identifier":"punitha","password":"admin123"}, { type: 'LOGIN_USER_SUCCESS' })
  //     expect(loginState.loggedIn).toEqual(true);
  // });

  it('should handle LOGIN_USER_SUCCESS', () => {
    expect(
      loginReducer(initialState,
      {
        type: 'LOGIN_USER_SUCCESS'
      })
    ).toMatchSnapshot()
  })
  it('should handle the Action  LOGIN_USER_ERROR', () => {
      expect(
        loginReducer(initialState,
          {
              loggedIn: true,
          },
          {
            type: 'LOGIN_USER_ERROR',
          },
        ),
      ).toEqual({
          "loggedIn": false,"error":null, "login":[]
      });
  });
});