import deleteCartReducer from '../deleteCartReducer';
import {initialState} from '../deleteCartReducer';
describe("CART delete Reducer", ()=>{
  //Reducers
  // it('should set true for loggedIn for LOGIN_USER_SUCCESS action', () => {
  //     let loginState = loginReducer({"identifier":"punitha","password":"admin123"}, { type: 'LOGIN_USER_SUCCESS' })
  //     expect(loginState.loggedIn).toEqual(true);
  // });

  it('should handle CARTDELETE_LIST_SUCCESS', () => {
    expect(
      deleteCartReducer(initialState,
      {
        type: 'CARTDELETE_LIST_SUCCESS'
      })
    )
  })
  it('should handle the Action  CARTDELETE_LIST_ERROR', () => {
    expect(
      deleteCartReducer(initialState,
        {
          deleted: true,
        },
        {
          type: 'CARTDELETE_LIST_ERROR',
        },
      ),
    ).toEqual({
        "deleted": false,"error":null
    });
  });
});