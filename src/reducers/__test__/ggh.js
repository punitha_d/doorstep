test('should submit form on save click when all data is filled properly', async () => {
    const MOCKED_DATA = {
        identifier: 'name 1',
        username: 'desc 1'
    };
    const NEW_NAME = 'new name';
    const onSubmit = jest.fn();
    jest.useFakeTimers();
    const { queryByText, queryByPlaceholderText } = render(
        <Provider store={store}>
        <BrowserRouter>  
            <Login  onSubmit={onSubmit}/>
        </BrowserRouter>
    </Provider>

    );

    const nameInput = queryByPlaceholderText("enterusername");
    fireEvent.change(nameInput, { target: { value: NEW_NAME } });
    fireEvent.click(queryByText('Submit'));

    await waitFor(
        () => expect(onSubmit).toHaveBeenCalledWith({
            identifier: NEW_NAME,
            username: MOCKED_DATA.username
        })
    );
    jest.useRealTimers();
});
