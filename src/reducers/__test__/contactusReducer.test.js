import contactusReducer from '../contactusReducer';
import {initialState} from '../contactusReducer';

describe("Contactus Reducer", ()=>{
  //Reducers
  // it('should set true for loggedIn for LOGIN_USER_SUCCESS action', () => {
  //     let loginState = loginReducer({"identifier":"punitha","password":"admin123"}, { type: 'LOGIN_USER_SUCCESS' })
  //     expect(loginState.loggedIn).toEqual(true);
  // });

  it('should handle CONTACTUS_SUCCESS', () => {
    expect(
      contactusReducer(initialState,
      {
        type: 'CONTACTUS_SUCCESS'
      })
    )
  })
  it('should handle the Action CONTACTUS_ERROR', () => {
    expect(
      contactusReducer(initialState,
        {
            contact: true,
        },
        {
          type: 'CONTACTUS_ERROR',
        },
      ),
    ).toEqual({
        "contact": false,"error":null, "contactus":[]
    });
  });
});