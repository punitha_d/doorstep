import aboutusReducer from '../aboutusReducer';
import {initialState} from '../aboutusReducer';
describe("Aboutus Reducer", ()=>{
  //Reducers
  // it('should set true for loggedIn for LOGIN_USER_SUCCESS action', () => {
  //     let loginState = loginReducer({"identifier":"punitha","password":"admin123"}, { type: 'LOGIN_USER_SUCCESS' })
  //     expect(loginState.loggedIn).toEqual(true);
  // });

  it('should handle ABOUTUS_SUCCESS', () => {
    expect(
      aboutusReducer(initialState,
      {
        type: 'ABOUTUS_SUCCESS'
      })
    )
  })
  it('should handle the Action  ABOUTUS_ERROR', () => {
    expect(
      aboutusReducer(initialState,
        {
            about: true,
        },
        {
          type: 'ABOUTUS_ERROR',
        },
      ),
    ).toEqual({
        "about": false,"error":null, "aboutus":[]
    });
});
});