import { initialize } from 'redux-form';
import * as types from '../actions/types';

export default function deleteCart(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.CARTDELETE_LIST_SUCCESS: 
        return { ...state, response, deleted: true};
    case types.CARTDELETE_LIST_ERROR:
      return { ...state, response, deleted: false};
    case types.REMOVE_CART_NOTIFICATION:
      return{...state, deleted:false };
    default:
      return state;
  }
};