import * as types from '../actions/types';

//initialstate is only for test
export const initialState = { 
  loggedIn: false,
    login: []
};

export default function login(state = initialState, action) {
  const response = action.response;
  switch(action.type) {
    case types.LOGIN_USER_SUCCESS:
      return { ...state, response, loggedIn: true};
    case types.LOGIN_USER_ERROR:
      const errresponse = action.payload;
      return { ...state, loggedIn:false, errresponse};
    default:
      return state;
  }
};