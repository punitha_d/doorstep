import * as types from '../actions/types';

export default function categoriesReducer(state = [], action) {
  let response = action.payload;
  switch(action.type) {
    case types.CATEGORIES_LIST_SUCCESS:
      return { ...state, response };
    case types.CATEGORIES_LIST_ERROR:
      return { ...state, response };
    default:
      return state;
  }
}

