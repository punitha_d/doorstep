import * as types from "../actions/types";
export const initialState = { 
    about:false,
    aboutus: [],
  error: null
};
export default function aboutus(state = initialState, action){
    let response = action.payload;
    switch(action.type){
        case types.ABOUTUS_SUCCESS:
            return {state, response, about:true};
        case types.ABOUTUS_ERROR:
            return {state, response, about:false};
        default:
            return state;
    }

}