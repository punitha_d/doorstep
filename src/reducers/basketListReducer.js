import * as types from "../actions/types";
export default function basketList(state= [], action){
    let response = action.payload;
    switch(action.type){
        case types.BASKET_LIST_SUCCESS:
            return (state, response);
        case types.BASKET_LIST_ERROR:
            return (state, response);
        default:
            return state;
    }
}

