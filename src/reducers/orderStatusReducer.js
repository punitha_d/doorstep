import * as types from "../actions/types";

export default function orderStatus(state = [], action){
    let response = action.payload;
    switch(action.type){
        case types.ORDERSTATUS_LIST_SUCCESS:
            return {state, response, orderstatus: true}; 
        case types.ORDERSTATUS_LIST_ERROR:
            return {state, response, orderstatus: false};
        case types.ORDERSTATUS_LIST_NOTIFICATION:
            return {state, orderstatus: false};    
        default:
            return state;
    }

}