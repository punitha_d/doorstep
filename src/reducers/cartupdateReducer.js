import * as types from '../actions/types';

export default function cartupdate(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.CART_UPDATE_SUCCESS: 
        return { ...state, response,  success: true };
    case types.CART_UPDATE_ERROR:
      return { ...state, response, success: false  };
      case types.CART_UPDATE_NOTIFICATION:
        return { ...state, success: false };
    default:
      return state;
  }
};