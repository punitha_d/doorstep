import * as types from '../actions/types';

//initialstate only for test
export const initialState = { 
  registered:false,
  register: [],
error: null
};


export default function register(state = [], action) {
  let response = action.response;
  switch(action.type) {
    case types.REGISTER_USER_SUCCESS:
      return { ...state, response, registered:true };
    case types.REGISTER_USER_ERROR:
      return { ...state, response, registered:false };
    default:
      return state;
  }
}