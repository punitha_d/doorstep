import * as types from '../actions/types';

export default function addTocart(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.ADDTOCART_LIST_SUCCESS:
      return { ...state, response, success: true };
    case types.ADDTOCART_LIST_ERROR:
      return { ...state, response };
      case types.ADDTOCART_LIST_NOTIFICATION:
        return { ...state, success: false };
    default:
      return state;
  }
};