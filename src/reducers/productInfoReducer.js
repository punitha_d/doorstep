import * as types from '../actions/types';

export default function productInfo(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.PRODUCTSINFO_LIST_SUCCESS: 
        return { ...state, response};
    case types.PRODUCTSINFO_LIST_ERROR:
      return { ...state, response };
    default:
      return state;
  }
};