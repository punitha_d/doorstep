import * as types from '../actions/types';
export default function editProfile(state = [], action) {
  const response = action.payload;
  switch(action.type) {
    case types.EDIT_PROFILE_SUCCESS: 
        return { ...state, response, success: true};
    case types.EDIT_PROFILE_ERROR:
      return { ...state, response, success: false };
      case types.EDIT_PROFILE_NOTIFICATION:
        return { ...state, success: false };
    default:
      return state;
  }
};