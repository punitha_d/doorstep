import * as types from "../actions/types";
export const initialState = { 
    faq:false,
    faqs: [],
  error: null
};

export default function faqs(state = initialState, action){
    let response = action.payload;
    switch(action.type){
        case types.FAQS_SUCCESS:
            return {state, response, faq:true}; 
        case types.FAQS_ERROR:
            return {state, response, faq:false}; 
        default:
            return state;
    }

}