import * as types from "../actions/types";

export default function bankOffer(state = [], action){
    let response = action.payload;
    switch(action.type){
        case types.BANK_OFFER_SUCCESS:
            return (state, response); 
        case types.BANK_OFFER_ERROR:
            return (state, response);
        default:
            return state;
    }

}