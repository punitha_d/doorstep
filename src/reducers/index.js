import { combineReducers } from 'redux';
import register from './registerReducer';
import login from './loginReducer';
import categories from './categoriesReducer';
import cities from './citiesReducer';
import aboutus from './aboutusReducer';
import faqs from './faqsReducer';
import brands from './brandsListReducer';
import productsList from './productListReducer';
import productInfo from './productInfoReducer';
import addTocart from './addTocartReducer';
import bankOffer from './bankOfferReducer';
import editProfile from './editProfileReducer';
import getProfile from './getProfileReducer';
import cartList from './cartListReducer';
import contactus from './contactusReducer';
import basketList from './basketListReducer';
import deleteCart from './deleteCartReducer';
import orderStatus from './orderStatusReducer';
import orderList from './orderListReducer';
import orderExist from './orderExistReducer';
import cartUpdate from './cartupdateReducer';


const rootReducer = combineReducers({
  register, login, 
  categories, cities, 
  aboutus, brands, 
  productsList, productInfo, 
  addTocart, bankOffer,
  getProfile,editProfile,
  cartList, faqs,
  contactus, basketList,
  deleteCart,orderStatus,
  orderList, orderExist,
  cartUpdate
});


export default rootReducer;