import * as types from "../actions/types";
export const initialState = { 
    contact:false,
    contactus: [],
  error: null
};
export default function contactus(state = initialState, action){
    let response = action.payload;
    switch(action.type){
        case types.CONTACTUS_SUCCESS:
            return {state, response, contact:true}; 
        case types.CONTACTUS_ERROR:
            return {state, response, contact:false};
        default:
            return state;
    }

}