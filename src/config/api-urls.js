import config from "../config";
export const REGISTER_URL = `${config.BASE_URL}/auth/local/register`;
export const LOGIN_URL = `${config.BASE_URL}/auth/local`;
export const CATEGORIES_URL = `${config.BASE_URL}/categories`;
export const CITIES_URL = `${config.BASE_URL}/available-cities`;
export const PRODUCTS_URL = `${config.BASE_URL}/products`;
export const BRANDS_URL = `${config.BASE_URL}/brands`;

//page url

export const ABOUTUS_URL = `${config.BASE_URL}/pages/`;
export const FAQS_URL = `${config.BASE_URL}/FAQS/`;
export const CONTACTUS_URL = `${config.BASE_URL}/Contactuses/`;
export const TERMS_URL = `${config.BASE_URL}/pages/?title=About Doorstep`;
export const PRIVACY_URL = `${config.BASE_URL}/pages/?title=About Us`;


export const USER_URL = `${config.BASE_URL}/users`;
export const ADDTOCART_URL =  `${config.BASE_URL}/order-details`;
export const BASKET_URL =  `${config.BASE_URL}/order-details/count?status=Incart`;
export const BANK_OFFER_URL =  `${config.BASE_URL}/bank-offers`;
export const PROFILE_URL =  `${config.BASE_URL}/users/`;
export const CARTLIST_URL =  `${config.BASE_URL}/order-details?status=Incart`;
export const CARTDELETE_URL =  `${config.BASE_URL}/order-details/`;
export const CARTUPDATE_URL =  `${config.BASE_URL}/order-details/`;
