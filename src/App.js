import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'

import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './components/Homepage';
import LoginPage from './components/Login/loginPage';
import RegisterPage from './components/Register/registerPage';
import Aboutus from './components/Mainpage/aboutus';
import Privacy from './components/Mainpage/privacy';
import Terms from './components/Mainpage/terms';
import News from './components/Mainpage/news';
import Faqs from './components/Mainpage/faqs';
import Contactus from './components/Mainpage/contactus';
import ProductList from './components/Mainpage/productslist';
import ProductInfo from './components/Mainpage/productinfo';
import Dashboardpage from './components/Dashboard/dashboard';
import Myaccount from './components/Dashboard/myaccount';
import EditProfile from './components/Dashboard/editProfile';
import Cartlist from './components/Dashboard/cartlist';
import Orderhistory from './components/Dashboard/order-history';
import Checkout from './components/Dashboard/checkout';
import Search from './components/Search/search';
import configureStore from "./store/configureStore";
import Navigation from "./components/Navigation"
class App extends Component {
  render() {
    return (
      <Provider store={configureStore()}>
      <BrowserRouter>
        <div>
        <Header />
        <Navigation />
          <Switch>
            <Route path='/' exact={true} component={HomePage} />
            <Route path='/login' component={LoginPage} />
            <Route path='/register' component={RegisterPage} />
            <Route path='/products-list' exact component={ProductList} />
            <Route path='/products-info' exact component={ProductInfo} />
            <Route path='/dashboard' component={Dashboardpage} />
            <Route path='/myaccount' component={Myaccount} />
            <Route path='/edit-profile' component={EditProfile} />
            <Route path='/mycart' component={Cartlist} />
            <Route path='/order-history' component={Orderhistory} />
            <Route path='/aboutus' component={Aboutus} />
            <Route path='/privacy' component={Privacy} />
            <Route path='/terms' component={Terms} />
            <Route path='/news' component={News} />
            <Route path='/faqs' component={Faqs} />
            <Route path='/contactus' component={Contactus} />
            <Route path='/checkout' component={Checkout} />
            <Route path='/search' component={Search} />
          </Switch>
        <Footer />
        </div>
      </BrowserRouter>
      </Provider>
    );
  }
}

export default App;