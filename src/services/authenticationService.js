import axios from "axios";
import {
        REGISTER_URL, 
        LOGIN_URL, 
        CATEGORIES_URL, 
        CITIES_URL, 
        ABOUTUS_URL, 
        BRANDS_URL,
        PRODUCTS_URL,
        ADDTOCART_URL,
        BANK_OFFER_URL,
        PROFILE_URL,
        CARTLIST_URL,
        FAQS_URL,
        CONTACTUS_URL,
        BASKET_URL,
        CARTDELETE_URL,
        CARTUPDATE_URL
      } from "../config/api-urls";


export const registerUserService = (params) => {
  return axios.post(REGISTER_URL,params.user)
};

export const loginUserService = (params) => {
return axios
  .post(LOGIN_URL, {
      identifier: params.username.identifier,
      password: params.username.password
  })
  .catch(error => {
    // Handle error.
    console.log('An error occurred:', error);
  });
};

export const categoriesListService = (payload) => {
  return axios.get(CATEGORIES_URL, payload)
  
};
//Footer 
//cities we serve api call
export const citiesListService = (payload)=>{
  return axios.get(CITIES_URL, payload)
}
//Brands list api call
export const brandsListService = (payload)=>{
  return axios.get(BRANDS_URL, payload)
}

//FAQs page
export const faqsService = ()=>{
  return axios.get(FAQS_URL)
}
//About us page
export const aboutusService = ()=>{
  return axios.get(ABOUTUS_URL)
}
//About us page
export const contactusService = ()=>{
  return axios.get(CONTACTUS_URL)
}

//Products list page
export const productsListService = ()=>{
  const params = new URLSearchParams(window.location.search)
  let id = params.get('id')
  let bid = params.get('brandid')
  if(id){
    return axios.get(`${PRODUCTS_URL}?category.id=${id}`)
  }else{
    return axios.get(`${PRODUCTS_URL}?brand.id=${bid}`)
  }
}
//Single product page
export const productsInfoService = ()=>{
  const params = new URLSearchParams(window.location.search)
  let id = params.get('id')
  return axios.get(`${PRODUCTS_URL}?id=${id}`)
}

export const addTocartService = (payload)=>{
 return axios.post(ADDTOCART_URL, 
  {
    productid:payload.productid,
    productname:payload.productname,
    userid:payload.userid,
    orderquantity:payload.orderquantity,
    price:payload.price,
    total:payload.total,
    status:payload.status,
    action:payload.onSuccess
  })
}

//Basket list
export const basketListService = ()=>{
  return axios.get(BASKET_URL)
}


//Bank offers

export const bankOfferService = ()=>{
  return axios.get(BANK_OFFER_URL)
}

// fetch user profile
export const getProfileService = (payload)=>{
  return axios.get(`${PROFILE_URL}${payload.userid}`)
}

export const editProfileService = (payload)=>{
  return axios.put(`${PROFILE_URL}${payload.userid}`, 
    {
      firstname:payload.firstname,
      lastname:payload.lastname,
      mobileno:payload.mobileno,
      street:payload.street,
      city:payload.city,
      state:payload.state,
      country:payload.country
    })
}

//cart list
export const cartListService = ()=>{
  let userdetail = JSON.parse(localStorage.getItem('user'))
  let userid = userdetail.data.user.id;

  return axios.get(`${CARTLIST_URL}&userid=${userid}`)
}

//CART UPDATE
export const cartupdateService = (payload)=>{
  return axios.put(`${CARTUPDATE_URL}${payload.prodid}`, 
    {
      total:payload.total,
      orderquantity:payload.orderquantity
    })
}



 
//DELETE CART ITEMS
export const deleteCartService = (payload)=>{
  return axios.delete(`${CARTDELETE_URL}${payload.id}`)
 }

 export const orderListService = ()=>{
  let userdetail = JSON.parse(localStorage.getItem('user'))
  return axios.get(`${CARTDELETE_URL}?userid=${userdetail.data.user.id}`)

}

export const orderStatusService = (payload)=>{
  return axios.put(`${CARTDELETE_URL}${payload.id}`, 
    {
      status:payload.status
    })
}

export const orderExistService = ()=>{
  let userdetail = JSON.parse(localStorage.getItem('user'))
  const urlParams = new URLSearchParams(window.location.search);
  const prodid = urlParams.get('id'); 
  return axios.get(`${CARTLIST_URL}&userid=${userdetail.data.user.id}&productid=${prodid}`)
}

